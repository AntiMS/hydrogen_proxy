from http.client import HTTPMessage
from io import BytesIO
import traceback

from .util.templates import template_environment


def error_response(request, error):
    """
    Returns an HTTPResponse of an HTML page presenting details of the given
    error message.
    """
    content = template_environment.get_template("error.html").render(
            stacktrace=traceback.format_exc()).encode("UTF-8")

    headers = HTTPMessage()
    headers["Server"] = "Hydrogen Proxy"
    headers["Content-Type"] = "text/html"
    headers["Content-Length"] = str(len(content))
    content = BytesIO(content)
    return request.create_response(500, "Internal Server Error", "HTTP/1.1",
                                   headers, content)
