from .base import Body


class SimpleBody(Body):
    """
    A body without a transfer encoding.
    """
    def __init__(self, content, *args, **kwargs):
        """
        Parameters
        ==========
        content
            The readable object containing the body of the request/response.
        """
        self._content = content
        super().__init__(*args, **kwargs)

    def _get_from_stream(self, s):
        return self._content.read(s)

    def close(self):
        super().close()
        self._content.close()
