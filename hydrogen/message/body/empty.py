class EmptyBody:
    """
    An empty body.
    """
    def __init__(self, content, length=None):
        self._content = content
        self.length = 0

    def read(self, s):
        return b""

    def close(self):
        self._content.close()
