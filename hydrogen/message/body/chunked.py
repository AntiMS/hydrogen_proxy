from .simple import SimpleBody


class ChunkedBody(SimpleBody):
    """
    A body with a transfer encoding of "chunked".
    """
    def __init__(self, *args, **kwargs):
        self._done = False
        self._chunk_remaining = None
        super().__init__(*args, **kwargs)

    def _start_chunk(self):
        line = self._content.readline()
        self._chunk_remaining = int(line, 16)
        if self._chunk_remaining == 0:
            self._done = True
            if self._content.read(2) != b"\r\n":
                raise Exception("Malformed chunked encoding.")
            # TODO: handle footers

    def _end_chunk(self):
        if self._content.read(2) != b"\r\n":
            raise Exception("Malformed chunked encoding.")
        self._chunk_remaining = None

    def _get_from_stream(self, s):
        if self._chunk_remaining is None:
            self._start_chunk()
        if self._done:
            return b""
        o = self._content.read(min(s, self._chunk_remaining))
        self._chunk_remaining -= len(o)
        if self._chunk_remaining == 0:
            self._end_chunk()
        return o
