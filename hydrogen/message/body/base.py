import io

from hydrogen.conf import settings


class Body:
    """
    Represents the body of a request or response.

    Subclasses should define the _get_from_stream() method.
    """
    def __init__(self, length=None):
        """
        Parameters
        ==========
        length
            The length of the body, if known before instantiating this body
            object.
        """
        self.length = length
        self._buff = io.BytesIO()
        # How far into the total request the current beginning of the buffer
        # starts.
        self._buff_offset = 0
        self._fill_buffer()

    def _clean_buff(self):
        """
        Dumps old, already read buffer contents to free up memory.
        """
        tell = self._buff.tell()
        if tell == 0:
            return
        new_buff = io.BytesIO(self._buff.getvalue()[tell:])
        new_buff_offset = self._buff_offset + tell
        del self._buff
        self._buff = new_buff
        self._buff_offset = new_buff_offset

    def _fill_buffer(self):
        """
        Fills this input object's buffer.
        """
        self._clean_buff()

        if self.length is not None:
            remaining = self.length - self._buff_offset - len(
                self._buff.getvalue())
        else:
            remaining = settings.CHUNK_SIZE

        pos = self._buff.tell()
        self._buff.seek(len(self._buff.getvalue()))
        while remaining > 0:
            towrite = self._get_from_stream(remaining)
            if len(towrite) == 0:
                self.length = len(self._buff.getvalue()) + self._buff_offset
                break
            self._buff.write(towrite)
            remaining -= len(towrite)
        self._buff.seek(pos)

    def read(self, s):
        """
        Reads up to s bytes from the HTTP message body.
        """
        o = b""
        tell = self._buff_offset + self._buff.tell()
        while len(o) < s:
            if len(self._buff.getvalue()) - self._buff.tell() < s - len(o):
                self._fill_buffer()
                if self.length is not None:
                    s = min(s, self.length - tell)
            o += self._buff.read(s - len(o))
        return o

    def close(self):
        """
        Closes the body.
        """
        del self._buff
