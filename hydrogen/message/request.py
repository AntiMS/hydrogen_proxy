from urllib.parse import urlparse

from .transaction import HTTPTransaction
from .body.empty import EmptyBody
from .body.simple import SimpleBody
from .response import HTTPResponse


class HTTPRequest(HTTPTransaction):
    """
    Represents an HTTP request.
    """
    ATTRS = ('client_address', 'method', 'url')

    def __init__(self, client_address, method, url, *args, **kwargs):
        """
        Parameters
        ==========
        client_address
            (host, port) of the client.

        method
            'GET', 'POST', etc.

        url
            The url as a string.
        """
        self.client_address = client_address
        self.method = method
        self._url = urlparse(url)

        super().__init__(*args, **kwargs)

    @property
    def url(self):
        """
        The request url as a string.
        """
        return self._url.geturl()

    @property
    def protocol(self):
        """
        The request protocol/scheme as a string.  "http" or "https".
        """
        return self._url.scheme

    @property
    def domain(self):
        """
        The domain of the request.
        """
        return self._url.hostname

    @property
    def port(self):
        """
        The port to which the request is to be made.
        """
        if self._url.port is None:
            return 80 if self.protocol == "http" else 443
        return self._url.port

    @property
    def path(self):
        """
        The path portion of the request url.
        """
        return self._url.path

    @property
    def query(self):
        """
        The query string portion of the request url.
        """
        return self._url.query

    @property
    def path_query(self):
        """
        The path and query string portion of the request url.
        """
        if self.query == '':
            return self.path
        else:
            return "%s?%s" % (self.path, self.query)

    def get_body(self, length=None):
        # TODO: Determine all rules which imply an empty body.
        if self.method == "GET" or length == 0:
            return EmptyBody(self.content, length=length)
        return SimpleBody(self.content, length=length)

    def get_status_line(self, proxy=False):
        path = self.url if proxy else self.path_query
        return b" ".join([s.encode("utf-8") for s in [self.method, path,
                          self.http_version]])

    def create_response(self, *args, **kwargs):
        return HTTPResponse(self, *args, **kwargs)
