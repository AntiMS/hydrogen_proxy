import io


class CatInput:
    """
    Concatenates multiple inputs and/or bytes' into a single input stream.
    """
    def __init__(self, *args):
        """
        Parameters
        ==========
        *args
            All input objects from which to read.  Any of these may be a bytes
            object.
        """
        self._inputs = [io.BytesIO(a) if isinstance(a, bytes) else a for a in
                        args]

    def read(self, s):
        o = b""
        while len(o) < s and len(self._inputs) > 0:
            chunk = self._inputs[0].read(s)
            if len(chunk) == 0:
                if hasattr(self._inputs[0], "close"):
                    self._inputs[0].close()
                self._inputs = self._inputs[1:]
                continue
            o += chunk
        return o

    def close(self):
        for inp in self._inputs:
            if hasattr(inp, "close"):
                inp.close()
        self._inputs = ()

    @property
    def length(self):
        o = 0
        for inp in self._inputs:
            if hasattr(inp, "length"):
                o += inp.length
            elif hasattr(inp, "getvalue"):
                o += len(inp.getvalue())
            else:
                o = None
                break
        return o
