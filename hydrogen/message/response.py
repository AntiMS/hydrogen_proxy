from .transaction import HTTPTransaction
from .body.simple import SimpleBody
from .body.chunked import ChunkedBody
from .body.empty import EmptyBody


class HTTPResponse(HTTPTransaction):
    """
    Represents an HTTP response.
    """
    ATTRS = ('request', 'status', 'message')

    def __init__(self, request, status, message, *args, **kwargs):
        """
        Parameters
        ==========
        request
            The HTTPRequest instance for this HTTPResponse.

        status
            The server's response status.

        message
            The response message corresponding with the response status.
        """
        self.request = request
        self.status = status
        self.message = message

        super().__init__(*args, **kwargs)

    def get_body(self, length=None):
        # TODO: add empty body responses...
        trans_encoding = self.headers.get('Transfer-Encoding', None)
        if trans_encoding is not None and trans_encoding.lower() == "chunked":
            del self.headers['Transfer-Encoding']
            c = ChunkedBody
        elif length == 0:
            c = EmptyBody
        else:
            c = SimpleBody
        return c(self.content, length=length)

    def get_status_line(self, *args, **kwargs):
        return b" ".join([s.encode("utf-8") for s in [self.http_version,
                                                      str(self.status),
                                                      self.message]])
