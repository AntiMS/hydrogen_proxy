from .catinput import CatInput


class HTTPTransaction:
    """
    Represents an HTTP message such as a request or response.

    Subclasses should define methods get_body() and get_status_line().
    """
    def __init__(self, http_version, headers, content=None, body=None,
                 tags=None):
        """
        Parameters
        ==========
        http_version
            The HTTP version string.  (e.g. "HTTP/1.1")

        headers
            The message headers.

        content
            A file-like object from which to create a body object. Optional.
            Superceeded by the "body" argument.

        body
            The body object holding the contents of this transaction. Optional.
            If neither "content" or "body" is given, it is assumed the body of
            this transaction is empty.

        tags
            Optional. A set of strings providing some metadata about this
            transaction.
        """
        self.http_version = http_version
        self.headers = headers
        self.content = content

        length = self.headers.get('Content-Length', None)
        if length is not None:
            length = int(length)

        if body is None:
            self.body = self.get_body(length)
        else:
            self.body = body

        if tags is None:
            self.tags = set()
        else:
            self.tags = tags

    @property
    def length(self):
        """
        The length of the body.
        """
        return self.body.length

    def get_head(self, *args, **kwargs):
        """
        Returns the head portion of this transaction.
        """
        out = self.get_status_line(*args, **kwargs)
        out += b"\n"
        out += self.headers.as_string().encode("utf-8")
        out = out.replace(b"\n", b"\r\n")
        return out

    def get_input(self, head=True, *args, **kwargs):
        """
        Returns a (somewhat) file-like object from which the transaction can be
        read.

        Parameters
        ==========
        head
            Optional.  Defaults to True.  If True, includes the transaction
            head (status line and headers).  If False, only the body.
        """
        if head:
            return CatInput(self.get_head(*args, **kwargs), self.body)
        return self.body

    def clone(self, **kwargs):
        """
        Returns a clone of this transaction.

        Parameters
        ==========
        **kwargs
            Individual properties to replace in the resulting clone before
            returning.
        """
        attrs = self.ATTRS + ('http_version', 'headers', 'content', 'body',
                              'tags')
        for attr in attrs:
            if attr not in kwargs:
                kwargs[attr] = getattr(self, attr)
        return type(self)(**kwargs)
