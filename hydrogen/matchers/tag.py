from .base import BaseMatcher


class TagMatcher(BaseMatcher):
    """
    Matches if all tags given to the constructor are present on the request.
    """
    def __init__(self, *tags):
        """
        Parameters
        ==========
        *tags
            Tags against which to test.
        """
        self.tags = set(tags)

    def matches(self, request):
        return self.tags.issubset(request.tags)
