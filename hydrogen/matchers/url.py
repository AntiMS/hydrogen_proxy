from .regex import RegexMatcher


class URLMatcher(RegexMatcher):
    """
    Matches if the regex given to the constructor matches the request url.
    """
    def get_matchable_string(self, request):
        return request.url
