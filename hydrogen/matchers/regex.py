import re

from .base import BaseMatcher


class RegexMatcher(BaseMatcher):
    """
    Base class for matchers which use a single regex to determine if requests
    match.
    """
    def __init__(self, regex):
        if not hasattr(regex, 'match'):
            regex = re.compile(regex)
        self.regex = regex

    def matches(self, request):
        return bool(self.regex.search(self.get_matchable_string(request)))
