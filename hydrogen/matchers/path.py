from .regex import RegexMatcher


class PathMatcher(RegexMatcher):
    """
    Matches if the regex given to the constructor matches the request path.
    """
    def get_matchable_string(self, request):
        return request.path
