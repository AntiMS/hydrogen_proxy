from .regex import RegexMatcher


class ProtocolMatcher(RegexMatcher):
    """
    Matches if the regex given to the constructor matches the request protocol.

    TODO: A regex may not be the best way to handle this.  The protocol should
    always be "http" or "https".  Never anything else.  There's really no point
    in a regex.
    """
    def get_matchable_string(self, request):
        return request.protocol
