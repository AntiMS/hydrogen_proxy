class BaseMatcher(object):
    """
    Superclass of all matchers.
    """
    def matches(self, request):
        raise NotImplementedError

    def __or__(self, other):
        return OrCombinedMatcher(self, other)

    def __and__(self, other):
        return AndCombinedMatcher(self, other)

    def __invert__(self):
        return InvertingMatcher(self)


class InvertingMatcher(BaseMatcher):
    """
    A matcher which matches only if the single wrapped matcher does not match.
    """
    def __init__(self, matcher):
        self.matcher = matcher

    def matches(self, request):
        return not self.matcher.matches(request)


class CombinedMatcher(BaseMatcher):
    """
    Base class for matchers which represent "combinations" of other, wrapped
    matchers.
    """
    def __init__(self, *args):
        self.matchers = args


class OrCombinedMatcher(CombinedMatcher):
    """
    A matcher which matches if any of its wrapped matchers match.
    """
    def matches(self, request):
        for matcher in self.matchers:
            if matcher.matches(request):
                return True


class AndCombinedMatcher(CombinedMatcher):
    """
    A matcher which matches if all of its wrapped matchers match.
    """
    def matches(self, request):
        for matcher in self.matchers:
            if not matcher.matches(request):
                return False
        return True
