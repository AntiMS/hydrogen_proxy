from .regex import RegexMatcher


class DomainMatcher(RegexMatcher):
    """
    Matches if the regex given to the constructor matches the request domain.
    """
    def get_matchable_string(self, request):
        return request.domain
