import socket
import ssl
from http.client import parse_headers

from hydrogen.conf import settings


class HTTPSender:
    """
    A sender which sends an HTTP request directly to the server via TCP.
    """
    def get_socket(self):
        """
        Creates/returns the socket.
        """
        return socket.socket()

    def get_input(self, request):
        """
        Creates the input of content to send to the remote server.
        """
        return request.get_input()

    def connect(self, sock, request):
        """
        Causes the given socket to connect and returns the socket.
        """
        sock.connect((request.domain, request.port))
        return sock

    def ssl_wrap(self, sock, request):
        """
        Wraps the socket with an SSLSocket and returns the result.
        """
        context = ssl.create_default_context()
        host = request.domain
        if request.port != 443:
            host = host + ":" + 443
        return context.wrap_socket(sock, server_hostname=host)

    def __call__(self, request):
        if settings.VERBOSE:
            print("===================")
            print(request.get_head().strip().decode("utf-8"))
            print("...")

        input = self.get_input(request)

        sock = self.connect(self.get_socket(), request)

        if request.protocol == "https":
            sock = self.ssl_wrap(sock, request)

        while True:
            chunk = input.read(settings.CHUNK_SIZE)
            if len(chunk) == 0:
                break
            sock.sendall(chunk)

        content = sock.makefile("rb")
        status_line = content.readline().decode("iso-8859-1")
        status_line_split = status_line.strip().split(" ", 2)
        if len(status_line_split) == 2:
            http_version, status = status_line_split
            message = ""
        else:
            http_version, status, message = status_line_split
        headers = parse_headers(content)

        response = request.create_response(status, message, http_version,
                                           headers, content)

        if settings.VERBOSE:
            print(response.get_head().strip().decode("utf-8"))
            print("===================")

        return response
