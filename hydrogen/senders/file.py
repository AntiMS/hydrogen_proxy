from http.client import HTTPMessage
import os


class FileSender:
    """
    A sender which returns the contents of a single specified file.
    """
    def __init__(self, path, content_type="application/octet-stream"):
        """
        Parameters
        ==========
        path
            The path to the file. If a relative path, relative to Hydrogen's
            configuration directory.

        content_type
            Optional. The value of the Content-Type header in the response. If
            None, the Content-Type header is omitted. Defaults to
            "application/octet-stream".
        """
        self.path = path
        self.content_type = content_type

    def __call__(self, request):
        headers = HTTPMessage()
        headers["Server"] = "Hydrogen Proxy"
        if self.content_type is not None:
            headers["Content-Type"] = self.content_type
        headers["Content-Length"] = str(os.stat(self.path).st_size)

        content = open(self.path, "rb")

        return request.create_response(200, "Ok", "HTTP/1.1", headers, content)
