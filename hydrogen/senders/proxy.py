import base64
from http.client import parse_headers

from .http import HTTPSender


class ProxySender(HTTPSender):
    """
    A sender which forwards requests through an upstream HTTP proxy without
    making any direct DNS requests.
    """
    def __init__(self, host, port, auth_method=None, auth_credentials=None):
        """
        Parameters
        ==========
        host
            The host of the proxy to which to send the request.

        port
            The port on which to connect to the proxy

        auth_method
            Optional.  Currently, only "basic" is supported.  If not specified,
            (or None) defaults to no authentication.

        auth_credentials
            Only required if auth_method is specified.  Must be a tuple of the
            form (<username>, <password>) .
        """
        self.addr = (host, port)

        self.extra_headers = None
        if auth_method == "basic":
            if len(auth_credentials) != 2:
                raise Exception("Credentials provided to ProxySender do not "
                                "conform to requirements of \"basic\" proxy "
                                "authentication method.")
            data = base64.encodestring(":".join(auth_credentials).
                                       encode("UTF-8").replace(b"\n", b""))
            data = data.decode("ASCII")
            self.extra_headers = {"Proxy-Authorization": "Basic " + data}

    def get_input(self, request):
        http = (request.protocol == "http")
        if http and hasattr(self.extra_headers, "items"):
            for k, v in self.extra_headers.items():
                del request.headers[k]
                request.headers[k] = v
        return request.get_input(proxy=http)

    def connect(self, sock, request):
        sock.connect(self.addr)
        if request.protocol == "https":
            msg = b"CONNECT "
            msg += request.domain.encode() + b":" + str(request.port).encode()
            msg += b" HTTP/1.1\r\n"
            msg += b"User-Agent: HydrogenProxy/1.0\r\n"
            if hasattr(self.extra_headers, "items"):
                for k, v in self.extra_headers.items():
                    msg += k.encode() + b": " + v.encode() + b"\r\n"
            msg += b"\r\n"
            sock.sendall(msg)

            sfile = sock.makefile("rb")
            _, status, msg = sfile.readline().split(b" ", 2)
            status = int(status)
            parse_headers(sfile)

            if status != 200:
                raise Exception("ProxySender could not connect to upstream "
                                "proxy.")

        return sock
