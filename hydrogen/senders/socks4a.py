from .http import HTTPSender

try:
    import pystocking
except Exception:
    pystocking = None


class Socks4aSender(HTTPSender):
    """
    A sender which forwards requests through a SOCKS4a proxy without making any
    direct DNS requests.
    """
    def __init__(self, host, port):
        """
        Parameters
        ==========
        host
            The socks server hostname.

        port
            The socks server port.
        """
        if pystocking is None:
            raise Exception("Socks4aSender unavailable: Cannot import "
                            "pystocking.")

        self.host = host
        self.port = port

    def get_socket(self):
        return pystocking.Socks4aSocket(self.host, self.port)
