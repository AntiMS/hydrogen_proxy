import os
import os.path
from random import getrandbits

from hydrogen.conf import settings


__all__ = ['reset', 'get_cert', 'clear_certs_cache', 'generate_root_certs',
           'load_root']


def reset():
    """
    "Unloads" all loaded values for dealing with certificates. This is useful
    only when these values need to be changed at runtime.
    """
    global cacertfile, ckeyfile, certsdir, cakey, cacrt, cacrtstr, keystr, \
        key, crypto

    cacertfile = None
    ckeyfile = None
    certsdir = None

    cakey = None
    cacrt = None
    cacrtstr = None
    keystr = None
    key = None

    crypto = None


reset()


def init_base():
    """
    Initializes basic certificate-related parameters from settings.
    Specifically:

     * cacertfile
     * ckeyfile
     * certsdir
    """
    global cacertfile, ckeyfile, certsdir

    if cacertfile is None:
        cacertfile = getattr(settings, "ROOT_CERT_FILE",
                             os.path.join(settings.SETTINGS_DIR, "ca.pem"))
    if ckeyfile is None:
        ckeyfile = getattr(settings, "KEY_FILE",
                           os.path.join(settings.SETTINGS_DIR, "ckey.pem"))
    if certsdir is None:
        certsdir = getattr(settings, "CERTS_DIR",
                           os.path.join(settings.SETTINGS_DIR, "certs"))


def import_crypto():
    """
    pyOpenSSL can be *very* slow to import on some systems. This function
    imports something from pyOpenSSL and should only be called if necessary.
    """
    global crypto
    if crypto is None:
        from OpenSSL import crypto


def load_root():
    """
    Loads the root certificate.
    """
    global cakey, cacrt, cacrtstr, keystr, key

    loaded = True

    loaded = loaded and cakey is not None
    loaded = loaded and cacrt is not None
    loaded = loaded and cacrtstr is not None
    loaded = loaded and keystr is not None
    loaded = loaded and key is not None

    if loaded:
        return

    init_base()
    import_crypto()

    with open(cacertfile, 'rb') as f:
        ca = f.read()
        cakey = crypto.load_privatekey(crypto.FILETYPE_PEM, ca)
        cacrt = crypto.load_certificate(crypto.FILETYPE_PEM, ca)
        cacrtstr = crypto.dump_certificate(crypto.FILETYPE_PEM, cacrt)
    with open(ckeyfile, 'rb') as f:
        keystr = f.read()
        key = crypto.load_privatekey(crypto.FILETYPE_PEM, keystr)


def generate_root_certs():
    """
    Generates and saves (overwriting existing) root certificate files.
    """
    init_base()
    import_crypto()

    cakey = crypto.PKey()
    cakey.generate_key(crypto.TYPE_RSA, 1024)

    crt = crypto.X509()
    crt.set_version(0x2)
    crt.set_serial_number(getrandbits(20 * 8))
    crt.gmtime_adj_notBefore(0)
    crt.gmtime_adj_notAfter(60*60*24*365*25)
    crt.get_subject().CN = "Hydrogen Proxy"
    crt.get_subject().O = "Hydrogen Proxy Root Certificate"
    crt.get_subject().C = "US"
    crt.set_issuer(crt.get_subject())
    crt.set_pubkey(cakey)
    crt.sign(cakey, "sha256")

    crt.add_extensions([
        crypto.X509Extension(b"basicConstraints", True, b"CA:TRUE"),
        crypto.X509Extension(b"nsCertType", False, b"sslCA"),
        crypto.X509Extension(b"extendedKeyUsage", False,
                             b"serverAuth,clientAuth,emailProtection,"
                             b"timeStamping,msCodeInd,msCodeCom,msCTLSign,"
                             b"msSGC,msEFS,nsSGC"),
        crypto.X509Extension(b"keyUsage", True, b"keyCertSign, cRLSign"),
        crypto.X509Extension(b"subjectKeyIdentifier", False, b"hash",
                             subject=crt),
    ])

    cakey = crypto.dump_privatekey(crypto.FILETYPE_PEM, cakey)
    crt = crypto.dump_certificate(crypto.FILETYPE_PEM, crt)

    ckey = crypto.PKey()
    ckey.generate_key(crypto.TYPE_RSA, 1024)

    ckey = crypto.dump_privatekey(crypto.FILETYPE_PEM, ckey)

    with open(cacertfile, "wb") as f:
        f.write(cakey)
        f.write(crt)
    os.chmod(cacertfile, 0o400)

    with open(ckeyfile, "wb") as f:
        f.write(ckey)
    os.chmod(ckeyfile, 0o400)


def clear_certs_cache():
    """
    Empties the certificate cache.
    """
    init_base()

    for f in os.listdir(os.path.join(certsdir)):
        fp = os.path.join(certsdir, f)
        if f.endswith(".pem") and os.path.isfile(fp):
            os.remove(fp)


def get_cert(cn):
    """
    Returns the filename of a file containing a PEM certificate for the given
    common name. Generates the certificate if necessary.
    """
    load_root()

    path = os.path.join(certsdir, cn + ".pem")

    if not os.path.exists(path):
        r = crypto.X509Req()
        r.set_version(0x2)
        r.get_subject().CN = cn
        r.add_extensions([
            crypto.X509Extension(b"keyUsage", True,
                                 b"digitalSignature,keyEncipherment"),
            crypto.X509Extension(b"extendedKeyUsage", False,
                                 b"serverAuth,clientAuth"),
            crypto.X509Extension(b"basicConstraints", True, b"CA:FALSE"),
            crypto.X509Extension(b"subjectAltName", False,
                                 b"DNS:" + cn.encode("UTF-8")),
        ])
        r.set_pubkey(key)
        r.sign(key, "sha256")

        crt = crypto.X509()
        crt.set_version(r.get_version())
        crt.set_serial_number(getrandbits(20 * 8))
        crt.gmtime_adj_notBefore(0)
        crt.gmtime_adj_notAfter(60*60*24*365*25)
        crt.set_issuer(cacrt.get_subject())
        crt.set_subject(r.get_subject())
        crt.set_pubkey(r.get_pubkey())
        crt.add_extensions(r.get_extensions())
        crt.add_extensions([
            crypto.X509Extension(b"subjectKeyIdentifier", False, b"hash",
                                 subject=crt),
            crypto.X509Extension(b"authorityKeyIdentifier", False,
                                 b"keyid:always", issuer=cacrt),
        ])
        crt.sign(cakey, "sha256")

        cert = crypto.dump_certificate(crypto.FILETYPE_PEM, crt)

        with open(path, 'wb') as f:
            f.write(keystr)
            f.write(cert)
            f.write(cacrtstr)

    return path
