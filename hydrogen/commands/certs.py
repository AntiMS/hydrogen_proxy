__all__ = ['clearcerts', 'mkcert']


def clearcerts():
    """Empties Hydrogen Proxy's fake certificate cache."""
    from hydrogen.cert import clear_certs_cache
    clear_certs_cache()


def mkcert():
    """Generates Hydrogen's root certificate and key."""
    from hydrogen.cert import generate_root_certs
    generate_root_certs()
