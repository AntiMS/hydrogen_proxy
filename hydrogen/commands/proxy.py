__all__ = ['proxy']


def proxy():
    """Initializes the configured chains and runs the proxy."""
    from hydrogen.init import init_chains
    from hydrogen.conf import settings
    init_chains(settings.CHAINS)
