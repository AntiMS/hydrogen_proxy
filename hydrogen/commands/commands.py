__all__ = ['commands']


def commands():
    """Prints all available commands."""
    from hydrogen import commands
    print("Available commands:")
    print()
    maxlen = max([len(c) for c in commands.__all__])
    for command in commands.__all__:
        print("  {}{} - {}".format(command, " " * (maxlen - len(command)),
                                   getattr(commands, command).__doc__))
