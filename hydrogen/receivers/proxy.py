import ssl
import threading
from http.client import parse_headers
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn, ForkingMixIn

from hydrogen.conf import settings
from hydrogen.message import HTTPRequest
from hydrogen.cert import get_cert, load_root
from hydrogen.error import error_response


class ProxyRequestHandler(BaseHTTPRequestHandler):
    """
    Handles proxy communications with the client.
    """
    def do_request(self, command=None, path=None, request_version=None,
                   headers=None, rfile=None, wfile=None):
        if command is None:
            command = self.command
        if path is None:
            path = self.path
        if request_version is None:
            request_version = self.request_version
        if headers is None:
            headers = self.headers
        if rfile is None:
            rfile = self.rfile
        if wfile is None:
            wfile = self.wfile

        try:
            req = HTTPRequest(self.client_address, command, path,
                              request_version, headers, rfile)

            del req.headers["Connection"]
            req.headers["Connection"] = "Close"
            res = self.server.handler(req)
            del res.headers["Connection"]
            res.headers["Connection"] = "Close"

            inp = res.get_input()
        except Exception as e:
            res = error_response(req, e)
            del res.headers["Connection"]
            res.headers["Connection"] = "Close"

            inp = res.get_input()

        while True:
            c = inp.read(settings.CHUNK_SIZE)
            if len(c) == 0:
                inp.close()
                break
            wfile.write(c)

    def do_OPTIONS(self):
        self.do_request()

    def do_GET(self):
        self.do_request()

    def do_HEAD(self):
        self.do_request()

    def do_POST(self):
        self.do_request()

    def do_PUT(self):
        self.do_request()

    def do_DELETE(self):
        self.do_request()

    def do_TRACE(self):
        self.do_request()

    def do_CONNECT(self):
        """
        Establishes an SSL connection with the server and performs the
        handshake with the client acting as the server. In this way, Hydrogen
        Proxy establishes itself as a man-in-the-middle with access to the
        unencrypted traffic between client and server.
        """
        self.wfile.write(b"HTTP/1.1 200 Connection Established\r\nProxy-Agent:"
                         b" Hydrogen Proxy 0.1\r\n\r\n")

        host, port = self.path.split(":")
        port = int(port)

        sock = ssl.wrap_socket(self.request, certfile=get_cert(host),
                               server_side=True)
        rfile = sock.makefile('rb')
        wfile = sock.makefile('wb')

        raw_first_line = rfile.readline()
        first_line = raw_first_line.decode("utf-8")[:-2]
        command, path, version = first_line.split(" ", 2)

        url = "https://" + host + ("" if port == 443 else ":" +
                                   str(port)) + path

        self.do_request(command, url, version, parse_headers(rfile), rfile,
                        wfile)


class ProxyServer(HTTPServer):
    def __init__(self, handler,  *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.handler = handler


class ThreadedProxyServer(ThreadingMixIn, ProxyServer):
    pass


class ForkingProxyServer(ForkingMixIn, ProxyServer):
    pass


class ProxyReceiver:
    """
    A receiver which acts as a simple http proxy.
    """
    def __init__(self, handler, parallel_method="thread",
                 listen_address=("127.0.0.1", 8080)):
        self.parallel_method = parallel_method
        self.listen_address = listen_address
        self.handler = handler

    def init(self):
        # Loading the root certificate here ensures it doesn't have to be done
        # on every request as new processes are forked off.
        load_root()
        thread_target = {
            'syncronous': ProxyServer,
            'thread': ThreadedProxyServer,
            'fork': ForkingProxyServer,
        }[self.parallel_method](self.handler, self.listen_address,
                                ProxyRequestHandler).serve_forever
        thread = threading.Thread(target=thread_target)
        thread.start()
