from .proxy import ProxyReceiver as proxy


__all__ = ['proxy']
