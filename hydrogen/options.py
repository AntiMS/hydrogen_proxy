import argparse
import sys


__all__ = ['parser']


_EPILOG = 'Use the \'commands\' command to list available commands.'


class OptionsWrapper:
    """
    By default, the value returned from an ArgumentParser's parse_args() method
    returns None when any unrecognized attribute is requested from it.
    Instances of this class wrap such objects and cause them instead to raise
    AttributeErrors for unrecognized attribute names.

    Parameters
    ==========
    wrapped
        The object to wrap.
    """
    def __init__(self, wrapped):
        self.wrapped = wrapped

    def __getattr__(self, name):
        if name.upper() != name:
            return super().__getattr__(self, name)

        out = getattr(self.wrapped, name)
        if out is None:
            raise AttributeError("Option {} not found".format(name))
        return out

    def __repr__(self):
        return self.__class__.__name__

    @property
    def text_repr(self):
        """
        Returns a textual representation of the settings defined in this
        options object.
        """
        out = []
        for p in dir(self.wrapped):
            if p.startswith("_"):
                continue
            val = getattr(self.wrapped, p)
            if val is None:
                continue
            out.append("{} = {}".format(p.repr(val)))
        return "\n".join(out)


class HydrogenParser(argparse.ArgumentParser):
    """
    An argparse.ArgumentParser which sets up its own arguments and performs
    validation on the output parsed arguments. Also wraps the parsed arguments
    object with an ObjectWrapper.

    See this parser's usage output for information about arguments configured.
    """
    def __init__(self):
        super().__init__(epilog=_EPILOG)
        self.add_argument('-n', '--no-settingsdir', dest='NO_SETTINGS_DIR',
                          help='Do not add any settings directory to the '
                          'PYTHONPATH nor change the active directory on '
                          'startup.', default=False, action='store_true')
        self.add_argument('-d', '--settingsdir', dest='SETTINGS_DIR',
                          help='A directory to add to the PYTHONPATH and to '
                          'change into on startup.')
        self.add_argument('-s', '--settings', dest='SETTINGS_MODULE',
                          help='The module containing settings.')
        self.add_argument('COMMAND',
                          help='The command to be run. Run h2 commands to get '
                          'the list of available commands.')

    def parse_args(self, *args, **kwargs):
        """
        Parses the arguments, does a bit of validation (to ensure -n and -d are
        not both present) and returns an OptionsWrapper-wrapped arguments
        object.

        Parameters
        ==========
        *args, **kwargs
            See the arguments taken by argparse.ArgumentParser's parse_args()
            method.
        """
        out = super().parse_args(*args, **kwargs)

        if out.NO_SETTINGS_DIR and \
                getattr(out, "SETTINGS_DIR", None) is not None:
            self.error("-n and -d options are mutually exclusive.")
            sys.exit(1)

        return OptionsWrapper(out)


# The parser instance. Exported for use by other modules.
parser = HydrogenParser()
