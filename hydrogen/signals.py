import bisect


__all__ = ["Bus", "default_bus"]


class BusEntry:
    """
    Represents an entry in the list of callbacks registered to a bus.
    """
    def __init__(self, name, callback):
        """
        Parameters
        ==========
        name
            The name or, if it ends with a "*", a name prefix.

        callback
            The callback to call when an event is triggered whose "name"
            parameter matches the name passed to this constructor.
        """
        assert "*" not in name[:-1]
        self.prefix = name.endswith("*")
        if self.prefix:
            name = name[:-1]
        self.name = name
        self.callback = callback

    def matches(self, value):
        """
        Returns true if the given value matches the name or name prefix with
        which this BusEntry instance was instantiated.
        """
        if self.prefix:
            return value.startswith(self.name)
        return self.name == value

    def _get_name(self, value):
        if isinstance(value, BusEntry):
            value = value.name
        return value

    def __lt__(self, value):
        return self._get_name(self) < self._get_name(value)

    def __gt__(self, value):
        return self._get_name(self) > self._get_name(value)

    def __le__(self, value):
        return self._get_name(self) <= self._get_name(value)

    def __ge__(self, value):
        return self._get_name(self) >= self._get_name(value)

    def __eq__(self, value):
        return self._get_name(self) == self._get_name(value)

    def __ne__(self, value):
        return self._get_name(self) != self._get_name(value)

    def __repr__(self):
        return self.name + "*" if self.prefix else self.name


class Bus:
    """
    Implements a publish/subscribe model for message passing and event
    notification within Hydrogen Proxy. Messages are triggered via the
    "trigger" method. Each message has a "name" which is a string. Listeners
    are registered via the "listen" method. Registration also requires a "name"
    indicating which events are to be listened to. The listener name can be
    either an exact event name (as passed to "trigger") or an event name
    prefix indicating that all events which start with the given prefix should
    cause the associated listener to be called.
    """
    def __init__(self):
        self.entries = []

    def trigger(self, name, *args, **kwargs):
        """
        "Triggers" an event/message by name.

        Parameters
        ==========
        name
            The name of the event. Used to determine exactly which listener
            callbacks will be called. Passed to each called listener callback
            as well.

        *args, **kwargs
            Passed to each listener callback (along with the name.)
        """
        assert "*" not in name
        for e in self.entries[bisect.bisect(self.entries, name[0]):]:
            if e.matches(name):
                e.callback(name, *args, **kwargs)
            if e > name:
                break

    def listen(self, name, callback):
        """
        Registers a callback to be called when an event with the given name (or
        name prefix) is triggered.

        Parameters
        ==========
        name
            A string. Indicates which events are to be connected to the
            callback. If this value ends with a "*", all triggered events whose
            name starts with the value (sans "*") will cause the callback to be
            called. If the value does not end with "*", only triggered events
            whose name exactly matches this value will cause the callback to be
            called.

        callback
            A callable to be called when a matched event is triggered. Should
            take a "name" argument as well as any positional and/or keyword
            arguments expected to have been passed to the associated "trigger"
            method.
        """
        bisect.insort(self.entries, BusEntry(name, callback))


# The default bus. It will probably be quite uncommon that there's a use for
# using a bus aside from the default bus.
default_bus = Bus()
