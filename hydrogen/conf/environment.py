from os import environ


class EnvironmentLayer:
    """
    Serves as a settings layer. Pulls attributes from environment variables
    (optionally) with name prefixes.

    Parameters
    ==========
    prefix
        A string with which to prefix all requested attributes. (With a prefix
        of "BOB", the attribute "SUE" will be pulled from an environment
        variable named "BOB_SUE".) If not provided, no prefix will be added.
        (With no prefix, the attribute "SUE" will be pulled from an environment
        variable named "SUE".)
    """
    def __init__(self, prefix=None):
        self.prefix = prefix

    def __getattr__(self, name):
        if name.upper() != name:
            return super().__getattr__(name)

        if self.prefix is not None:
            name = self.prefix + "_" + name

        try:
            return environ[name]
        except KeyError:
            raise AttributeError(
                    "Setting '{}' not found in environment".format(name))

    def _all_attributes(self):
        """
        Returns a dictionary of all settings present in the environment.
        """
        out = []
        for k, v in environ.items():
            if k.upper() != k:
                continue

            if self.prefix is not None:
                if not k.startswith(self.prefix + "_"):
                    continue
                k = k[len(self.prefix)+1:]

            out.append((k, v))
        return dict(out)

    @property
    def text_repr(self):
        """
        Returns a textual representation of the settings in this module.
        """
        return "\n".join(["{} = {}".format(k, repr(v)) for k, v in
                          self._all_attributes().items()])

    def __repr__(self):
        return "{}(prefix={})".format(self.__class__.__name__,
                                      repr(self.prefix))
