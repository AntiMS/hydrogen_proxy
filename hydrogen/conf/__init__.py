__all__ = ['settings', 'add_layer']


class DictLayer:
    """
    Wraps a layer dict so its interface matches that of other layers.

    Parameters
    ==========
    layer_dict
        The layer dict to wrap.
    """
    def __init__(self, layer_dict):
        self._layer_dict = layer_dict

    def __getattr__(self, name):
        if name.upper() != name:
            return super().__getattr__(name)

        try:
            return self._layer_dict[name]
        except KeyError:
            raise AttributeError(
                    "Setting '{}' not found in dict layer".format(name))

    @property
    def text_repr(self):
        """
        Returns a textual representation of the settings in this module.
        """
        return "\n".join(["{} = {}".format(k, repr(v)) for k, v in
                          self._layer_dict.items()])

    def __repr__(self):
        return self.__class__.__name__


class Settings:
    """
    Wraps a series of "layers" of configuration. When an attribute is requested
    from this object, it searches from the most-recently-added layer to the
    least-recently-added.

    A layer may be a dictionary (in which case the keys in the dictionary will
    be considered the setting names) or any object with attributes. The search
    moves onto the next layer if the current layer is a dictionary and does not
    contain the requested setting as a key or is an object and attempting to
    get the attribute raises an AttributeError (rather than returning None or
    some such).

    If the requested attribute is not found in any layer, this settings object
    itself raises an AttributeError.

    Parameters
    ==========
    starting_layers
        An iteratble of layers to start with. Defaults to an empty list.
    """
    def __init__(self, starting_layers=None):
        self._layers = []

        if starting_layers is not None:
            self.add_layers(*starting_layers)

    def __getattr__(self, name):
        for l in self._layers:
            try:
                return getattr(l, name)
            except AttributeError:
                pass
        raise AttributeError("Setting '{}' not found".format(name))

    def add_layers(self, *layers):
        """
        Adds zero or more layers to this settings module.

        Parameters
        ==========
        *layers
            Layers to add. Adds from first to last.
        """
        layers = [DictLayer(l) if isinstance(l, dict) else l for l in layers]
        layers.reverse()
        self._layers = layers + self._layers

    def get_layers(self):
        """
        Returns an iterable of the layers currently added to the settings
        object.

        Returns
        =======
        The layers added to this settings object.
        """
        return tuple(self._layers)


# The default settings object for the running instance of Hydrogen Proxy.
settings = Settings()

# A reference to the default settings object's add_layer method.
add_layers = settings.add_layers

get_layers = settings.get_layers
