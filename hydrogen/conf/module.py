from importlib import import_module


class ModuleLayer:
    """
    Serves as a settings layer. Pulls attributes from a given Python module.

    Parameters
    ==========
    settings_module
        May be a string or Python module object. If a string, not resolved to a
        python module until the first attribute is requeted.
    """
    def __init__(self, settings_module="settings"):
        self._settings_module = settings_module
        self._settings_text = None

    def _import_module(self):
        if isinstance(self._settings_module, str):
            self._settings_module = import_module(self._settings_module)

        if self._settings_text is None:
            with open(self._settings_module.__file__, 'r') as f:
                self._settings_text = f.read()

    def __getattr__(self, name):
        if name.upper() != name:
            return super().__getattr__(name)

        self._import_module()

        return getattr(self._settings_module, name)

    @property
    def text_repr(self):
        """
        Returns the source of the module, cached in memory from when the module
        was loaded.
        """
        self._import_module()

        return self._settings_text

    def __repr__(self):
        self._import_module()

        return "{}({})".format(self.__class__.__name__,
                               repr(self._settings_module.__name__))
