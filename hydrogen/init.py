from hydrogen.helpers import InstantiableModule
from hydrogen.signals import default_bus


__all__ = ["init_chains"]


class InvalidModdefException(Exception):
    pass


def _parse_moddef(moddef):
    """
    Given a module definition, the details of the module definition in a
    consistent form. Module definitions may take any of the following forms:

    * <class>
    * <class_name>
    * (<class>, <args>, <kwargs>)
    * (<class>, <args>)
    * (<class>,)
    * (<class_name>, <args>, <kwargs>)
    * (<class_name>, <args>)
    * (<class_name>,)
    * {"class": <class>, "args": <args>, "kwargs": <kwargs>}
    * {"class": <class>, "args": <args>}
    * {"class": <class>, "kwargs": <kwargs>}
    * {"class": <class>}
    * {"class": <class_name>, "args": <args>, "kwargs": <kwargs>}
    * {"class": <class_name>, "args": <args>}
    * {"class": <class_name>, "kwargs": <kwargs>}
    * {"class": <class_name>}
    * InstantiableModule(class_=<class>, args=<args>, kwargs=<kwargs>)
    * InstantiableModule(class_=<class>, args=<args>)
    * InstantiableModule(class_=<class>, kwargs=<kwargs>)
    * InstantiableModule(class_=<class>)
    * InstantiableModule(class_=<class_name>, args=<args>, kwargs=<kwargs>)
    * InstantiableModule(class_=<class_name>, args=<args>)
    * InstantiableModule(class_=<class_name>, kwargs=<kwargs>)
    * InstantiableModule(class_=<class_name>)
    * InstantiableModule(<class>, <args>, <kwargs>)
    * InstantiableModule(<class_name>, <args>, <kwargs>)
    * ModuleReference(name=<name>)
    * ModuleReference(<name>)

    In addition to the above forms, moddef may be anything which has a
    ".get_module(process_args)" method which returns an instantiated module.

    In the above list, <class> is a reference to a class, <class_name> is the
    python module path and name to a class as a string (ex.
    "hydrogen.handlers.submod"), <args> is an iterable of positional arguments
    to pass to the associated class's constructor, <kwargs> is a dict of
    keyword arguments to pass to the class's constructor, and <name> is the
    name of an already-instantiated module.

    Return Value
    ============
    An InstantiableModule instance whose "get_module" method will instantiate
    the module itself.
    """
    if isinstance(moddef, dict):
        moddef = dict(moddef)
        moddef["class_"] = moddef["class"]
        del moddef["class"]
        return InstantiableModule(**moddef)
    elif hasattr(moddef, "__iter__") and len(moddef) > 0 and len(moddef) < 4:
        return InstantiableModule(*moddef)
    elif isinstance(moddef, str) or callable(moddef):
        return InstantiableModule(moddef)
    elif callable(getattr(moddef, "get_module", None)):
        return moddef
    raise InvalidModdefException(
            "An invalid module definition was encountered: " + repr(moddef))


def _replace_modules(val, chains, inst, anon):
    """
    Crawls a structure of positional or keyword arguments replacing module
    objects (non-destructively) with their corresponding modules. This function
    instantiates modules as necessary in the process.

    Parameters
    ==========
    val
        The value. Any value may be passed. Typically this value will be an
        iterable of positional arguments, a dict of keyword arguments, a module
        object, or any other value.

    chains
        A dict of named module definitions. If this method needs to instantiate
        a module, it may need to pull its definition from the chains object.

    inst
        A dict of all currently-instantiated non-anonymous modules in the
        chains. If this method needs to fetch an already-instantiated module,
        it will fetch it by name from this argument.

    anon
        A list of all currently-instantiated anonymous modules in the chains.
        This method will register any instantiated anonymous modules to the
        anon list.

    Return Value
    ============
    A tuple of length 2. The first elemnt of the return value is a modified
    copy of "val" with all module objects replaced either with the
    already-instantiated non-anonymous module referred to or with the module
    instantiated based on the class, args, and kwargs specified by the module
    itself. The second element of the return value is a list of all modules
    which were instantied or fetched from "inst" in the process of doing the
    replacements on "val".
    """
    children = []
    if getattr(val, "name", None) is not None:
        out = _get_module(val.name, chains, inst, anon)
        children.append(out)
    elif callable(getattr(val, "get_module", None)):
        out = _instantiate_module(None, val, chains, inst, anon)
        if isinstance(out, str):
            out = inst[out]
        anon.append(out)
        children.append(out)
    elif isinstance(val, str):
        # Just avoids iterating over strings as a performance optimization.
        out = val
    elif isinstance(val, dict):
        out = {}
        for k, v in val.items():
            out[k], cs = _replace_modules(v, chains, inst, anon)
            children += cs
    elif hasattr(val, "__iter__"):
        out = []
        for v in val:
            o, cs = _replace_modules(v, chains, inst, anon)
            out.append(o)
            children += cs
        if isinstance(val, tuple):
            out = tuple(out)
    else:
        out = val
    return out, children


def _instantiate_module(name, module, chains, inst, anon):
    """
    Instantiates and returns a module given the module's moddef.

    Parameters
    ==========
    name
        The name of the module or None if the module has no name.

    module
        An object which has a ".get_module(process_args)" method which returns
        a module instance.

    chains
        A dict of the definitions of all modules to. This function will also
        instantiate modules in the chains argument which are children of this
        module as necessary.

    inst
        A dict of all currently-instantiated non-anonymous modules from the
        chains object. This function will pull modules from the inst argument
        which are children of this module as necessary.

    anon
        A list of all currently-instantiated anonymous modules from the chains
        object.

    Return
    ======
    The module instantiated.
    """
    children = []
    mod_args = None
    mod_kwargs = None

    def process_args(args):
        nonlocal children, mod_args, mod_kwargs
        arg_list, child_list = _replace_modules(args, chains, inst, anon)
        children += child_list
        if isinstance(arg_list, dict):
            mod_kwargs = arg_list
        else:
            mod_args = arg_list
        return arg_list

    out = module.get_module(process_args)

    if name is None:
        signal_name = "module_created.<anon>"
    else:
        signal_name = "module_created." + name

    default_bus.trigger(signal_name, name, out, mod_args, mod_kwargs,
                        tuple(children))

    return out


def _get_module(name, chains, inst, anon):
    """
    Fetches a module by name from the list of already-instantiated
    non-anonymous modules or, if it is not found, instantiates the module as
    defined in the chains object and registers it to inst as well as all
    non-anonymous descendent modules to inst and all anonymous descendent
    modules to anon.

    Parameters
    ==========
    name
        The name (as a string) of the module to fetch or instantiate.

    chains
        A dict of the definitions of all modules.

    inst
        A dict of all already-instantiated non-anonymous modules.

    anon
        A list of already-instantiated anonymous modules.

    Return Value
    ============
    The module fetched or instantiated.
    """
    assert name is None or isinstance(name, str)
    if name not in inst:
        module = _parse_moddef(chains[name])

        if getattr(module, "name", None) is None:
            inst[name] = _instantiate_module(name, module, chains, inst, anon)
        else:
            inst[name] = _get_module(module.name, chains, inst, anon)

    return inst[name]


def init_chains(chains):
    """
    Given a dict of the defintions of all modules, instantiates all of the
    modules and afterword calls (without arguments) the "init" method of all
    modules which have one.

    Parameters
    ==========
    chains
        A dict of the definitons of all modules. Each key should be the name of
        a module and each value should be a moddef. (See the docstring of
        _parse_moddef() above for more information.)
    """
    inst = {}
    anon = []
    for k, v in chains.items():
        _get_module(k, chains, inst, anon)
    for v in inst.values():
        if hasattr(v, "init") and callable(v.init):
            v.init()
    for v in anon:
        if hasattr(v, "init") and callable(v.init):
            v.init()
    return inst
