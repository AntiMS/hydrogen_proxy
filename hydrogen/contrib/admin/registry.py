__all__ = ["Admin", "default_admin"]


_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"


class AdminModule:
    """
    Contains meta information about a module including the name, a reference to
    the instance itself, parent references, and child references.
    """
    def __init__(self, admin, name, instance, args, kwargs, children,
                 parents=None):
        """
        Parameters
        ==========
        admin
            The Admin instance to which this module belongs.

        name
            The name of the module.

        instance
            The module instance itself.

        args, kwargs
            Arguments passed to the module on instantiation.

        children
            An iterable of child AdminModules.

        parents
            Optional. A list of parent AdminModules. Defaults to an empty list.
        """
        self.admin = admin
        self.name = name
        self.instance = instance
        self.args = args
        self.kwargs = kwargs
        self.children = children
        self.parents = parents if parents is not None else []

    def is_child(self, arg):
        """
        Returns True if the given argument is either an AdminModule or instance
        which is a child of self.

        Parameters
        ==========
        arg
            The argument to test.
        """
        if isinstance(arg, AdminModule):
            return arg in self.children
        for c in self.children:
            if arg is c.instance:
                return True
        return False

    def __eq__(self, other):
        if self.name != other.name:
            return False
        for n in ("admin", "instance"):
            if getattr(self, n) is not getattr(other, n):
                return False
        # Checking parents would just create circular references.
        for n in ("children", "args", "kwargs"):
            svals = getattr(self, n)
            ovals = getattr(other, n)
            if isinstance(svals, dict):
                svals = svals.values()
                ovals = ovals.values()
            for m1, m2 in zip(svals, ovals):
                if m1 != m2:
                    return False
        return True

    def __repr__(self):
        return "AdminModule({}, {}, {}, {}, {}, {}, {})".format(
            *[repr(getattr(self, v)) for v in ("admin", "name", "instance",
                                               "args", "kwargs", "children",
                                               "parents")])


class Admin:
    """
    A registry to which modules are registered.
    """
    def __init__(self):
        self.reset()

    def reset(self):
        """
        "Empties" the registry. Unregisters all registered modules.
        """
        self.entry_points = []
        self._modules_by_name = {}
        self._modules_by_id = {}

    def register(self, name, instance, args, kwargs, children):
        """
        Registers a module.

        Parameters
        ==========
        name
            The name of the module. If None, a name will be assigned
            automatically.

        instance
            The module instance itself.

        args, kwargs
            Arguments passed to the module on instantiation.

        children
            An iterable of child modules.
        """
        children = list(children)

        if name is None:
            name = ""
            i = id(instance)
            while i > 0:
                name = _ALPHABET[i % len(_ALPHABET)] + name
                i = i // len(_ALPHABET)
            name = "mod_" + name
        assert name not in self._modules_by_name
        assert id not in self._modules_by_id

        out = AdminModule(self, name, instance, args, kwargs, children)
        for i, child in enumerate(out.children):
            for mod in self._modules_by_name.values():
                if child is mod.instance:
                    out.children[i] = mod
                    mod.parents.append(out)
                    if mod in self.entry_points:
                        self.entry_points.remove(mod)
        self.entry_points.append(out)
        self._modules_by_name[name] = out
        self._modules_by_id[id(out.instance)] = out

    def get_by_instance(self, instance):
        """
        Given an instance, returns the AdminModule for it (or None if the given
        instance has no associated AdminModule.)

        Parameters
        ==========
        instance
            The instance for which to retrieve an AdminModule.
        """
        try:
            return self._modules_by_id[id(instance)]
        except KeyError:
            return None

    def get_by_name(self, name):
        """
        Returns an AdminModule instance by name.

        Parameters
        ==========
        name
            The name of the module.
        """
        try:
            return self._modules_by_name[name]
        except KeyError:
            return None

    @property
    def modules(self):
        """
        Returns an iterable of all AdminModule instances registered.
        """
        return list(self._modules_by_name.values())

    def __repr__(self):
        return "Admin({})".format(id(self))


# The default admin instance.
default_admin = Admin()
