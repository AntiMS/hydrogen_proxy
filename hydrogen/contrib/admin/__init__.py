# For side-effect only.
from .register import admin_register_by_prefix


# Silence pyflakes.
assert admin_register_by_prefix


__all__ = []
