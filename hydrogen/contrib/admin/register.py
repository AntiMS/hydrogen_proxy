from hydrogen.signals import default_bus
from .registry import default_admin


__all__ = ["admin_register_by_prefix"]


def admin_register_by_prefix(admin=default_admin, signal_bus=default_bus,
                             prefix=""):
    """
    Listens to the given signal bus for all "module_created" signals for
    modules whose name has a given prefix and registers the associated module
    to the given admin instance.

    Parameters
    ==========
    admin
        The Admin instance to which to register modules. Optional. Defaults to
        the default admin.

    signal_bus
        The signal bus on which to listen. Optional. Defaults to the default
        signal bus.

    prefix
        The module name prefix. Only modules whose names begin with this prefix
        will be registered. Optional. Defaults to an empty string. (That is to
        say that by default, all module_created signals will cause registration
        to the given admin.)
    """
    def callback(signal_name, module_name, instance, args, kwargs, children):
        admin.register(module_name, instance, args, kwargs, children)

    signal_bus.listen("module_created." + prefix + "*", callback)


# Ensure that all modules (for which "module_created.*" events are issued) are
# registered to the default admin.
admin_register_by_prefix()
