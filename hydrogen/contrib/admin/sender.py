from http.client import HTTPMessage
from io import BytesIO

from hydrogen.conf import get_layers
from hydrogen.util.templates import template_environment
from .registry import default_admin


__all__ = ["AdminSender"]


def _home(admin, base_path, url):
    """
    A "view" in the AdminSender which displays a list of all registered
    modules as well as a second list of just receivers.

    Parameters
    ==========
    admin
        The Admin instance for which to display modules/receivers.

    base_path
        The base path (path prefix) of the AdminSender which delegated to this
        handler.

    url
        The url path via which this view was reached.
    """
    entry_points = list(admin.entry_points)
    entry_points.sort(key=lambda e: e.name)
    modules = list(admin.modules)
    modules.sort(key=lambda e: e.name)
    return template_environment\
        .get_template("contrib/admin/admin_home.html")\
        .render(base_path=base_path, receivers=entry_points, modules=modules)


def _module(admin, base_path, url):
    """
    A "view" in the AdminSender which displays details of a single registered
    module.

    Parameters
    ==========
    admin
        The Admin instance for which to display a module.

    base_path
        The base path (path prefix) of the AdminSender which delegated to this
        handler.

    url
        The url path via which this view was reached.
    """
    assert url.startswith("module/")
    assert len(["/" for c in url if c == "/"]) == 1
    module_name = url[len("module/"):]
    module = admin.get_by_name(module_name)
    assert module is not None

    return template_environment\
        .get_template("contrib/admin/admin_module_details.html")\
        .render(base_path=base_path, module=module)


def _settings_layers(admin, base_path, url):
    """
    A "view" in the AdminSender which displays the configuration layers in the
    settings object.

    Parameters
    ==========
    admin
        An Admin instance.

    base_path
        The base path (path prefix) of the AdminSender which delegated to this
        handler.

    url
        The url path via which this view was reached.
    """
    assert url == "settings"
    return template_environment\
        .get_template("contrib/admin/admin_settings.html")\
        .render(base_path=base_path, layers=get_layers())


# Registers the above "views" to specific url paths.
_ROUTES = {
    "": _home,
    "module/": _module,
    "settings": _settings_layers,
}


class AdminSender:
    """
    A sender which returns an interface by which details of modules registered
    to the running instance of Hydrogen Proxy may be introspected and/or
    modified.
    """
    def __init__(self, admin=default_admin, base_path=""):
        self.base_path = base_path.strip("/")
        self.admin = admin

    def __call__(self, request):
        req_path = request.path_query.strip("/")
        assert req_path.startswith(self.base_path)
        req_path = req_path[len(self.base_path):].strip("/")

        best_path = None
        best_handler = None
        for path, handler in _ROUTES.items():
            if req_path.startswith(path) and (best_path is None or
                                              len(path) > len(best_path)):
                best_path = path
                best_handler = handler

        if best_handler is None:
            best_handler = _ROUTES[""]

        page = best_handler(self.admin, self.base_path, req_path).encode(
                "UTF-8")

        headers = HTTPMessage()
        headers["Server"] = "Hydrogen Proxy"
        headers["Content-Type"] = "text/html"
        headers["content-Length"] = str(len(page))
        return request.create_response(200, "Ok", "HTTP/1.1", headers,
                                       BytesIO(page))
