import importlib


__all__ = ['module', 'InstantiableModule', 'ModuleReference']

BINARY_OPS = ('add', 'and', 'rmod', 'mul', 'or', 'pow', 'radd', 'rand', 'rmod',
              'rmul', 'ror', 'rpow', 'rsub', 'rxor', 'sub', 'xor')
UNARY_OPS = ('invert', 'neg', 'pos')


class BaseModule:
    """
    Base class for all types of modules which support any unary operations or
    binary operations with other modules. Using operators with instances of
    subclasses will return an instance of either UnaryOpModule or
    BinaryOpMdule. For more information about these objects, see their
    docstrings.
    """
    def __add__(self, value):
        return BinaryOpModule("add", self, value)

    def __and__(self, value):
        return BinaryOpModule("and", self, value)

    def __mul__(self, value):
        return BinaryOpModule("mul", self, value)

    def __or__(self, value):
        return BinaryOpModule("or", self, value)

    def __pow__(self, value):
        return BinaryOpModule("pow", self, value)

    def __radd__(self, value):
        return BinaryOpModule("radd", self, value)

    def __rand__(self, value):
        return BinaryOpModule("rand", self, value)

    def __rmod__(self, value):
        return BinaryOpModule("rmod", self, value)

    def __rmul__(self, value):
        return BinaryOpModule("rmul", self, value)

    def __ror__(self, value):
        return BinaryOpModule("ror", self, value)

    def __rpow__(self, value):
        return BinaryOpModule("rpow", self, value)

    def __rsub__(self, value):
        return BinaryOpModule("rsub", self, value)

    def __rxor__(self, value):
        return BinaryOpModule("rxor", self, value)

    def __sub__(self, value):
        return BinaryOpModule("sub", self, value)

    def __xor__(self, value):
        return BinaryOpModule("xor", self, value)

    def __invert__(self):
        return UnaryOpModule("invert", self)

    def __neg__(self):
        return UnaryOpModule("neg", self)

    def __pos__(self):
        return UnaryOpModule("pos", self)


class BinaryOpModule(BaseModule):
    """
    A module (which implements a ".get_module(process_args)" method) which
    wraps exactly two modules and keeps track of an operator to apply.  The
    value returned by the ".get_module(process_args)" method will be that
    resulting when the first wrapped module instance is resolved combined with
    that of the second wrapped module via the operator.
    """
    def __init__(self, op, mod1, mod2):
        """
        Parameters
        ==========
        op
            The name of the Python magic method (sans leading and trailing
            underscores) of the operator to apply.

        mod1, mod2
            Module instances which when resolved will supply the operands of
            the operator.
        """
        assert hasattr(mod1, "__{}__".format(op))
        self.op = op
        self.mod1 = mod1
        self.mod2 = mod2

    def get_module(self, process_args):
        """
        Resolves the wrapped modules and applies the operator to them and
        returns the result.

        Parameters
        ==========
        process_args
            A callable which must be passed by the caller. When passed a
            module, must return the resolved value of the module.

        Return Value
        ============
        The resolved value of mod1 op'd with that of mod2.
        """
        mod1 = process_args(self.mod1)
        mod2 = process_args(self.mod2)
        return getattr(mod1, "__{}__".format(self.op))(mod2)

    def __repr__(self):
        return "binmodule({}, {}, {})".format(self.op, repr(self.mod1),
                                              repr(self.mod2))


class UnaryOpModule(BaseModule):
    """
    A module (which implements a ".get_module(process_args)" method) which
    wraps exactly one module and keeps track of a unary operator to apply. The
    value returned by the ".get_module(process_args)" method will be that
    resulting when the operator is applied to the value objtained when the
    wrapped module is resolved.
    """
    def __init__(self, op, mod):
        """
        Parameters
        ==========
        op
            The name of the Python magic method (sans leading and trailing
            underscores) of the operator to apply.

        mod
            The module instance which when resolved will supply the operand of
            the operator.
        """
        assert hasattr(mod, "__{}__".format(op))
        self.op = op
        self.mod = mod

    def get_module(self, process_args):
        """
        Resolves the wrapped module and applies the operator to it and returns
        the result.

        Parameters
        ==========
        process_args
            A callable which must be passed by the caller. When passed a
            module, must return the resolved value of the module.

        Return Value
        ============
        The resolved value of mod with the operator applied to it.
        """
        return getattr(process_args(self.mod), "__{}__".format(self.op))()

    def __repr__(self):
        return "unmodule({}, {})".format(self.op, repr(self.mod))


class InstantiableModule(BaseModule):
    """
    Represents a module to be instantiated.
    """
    def __init__(self, class_, args=(), kwargs={}):
        """
        Parameters
        ==========
        class_
            A reference to a class or a class' Python path as a string (ex.
            "hydrogen.handlers.gunzip")

        args
            An iterable of positional arguments to pass to the class'
            constructor.

        kwargs
            A dict of keyword arguments to pass to the class' constructor.
        """
        if isinstance(class_, str):
            split = class_.split(".")
            package = importlib.import_module(".".join(split[:-1]))
            class_ = getattr(package, split[-1])
        self.class_ = class_
        self.args = args
        self.kwargs = kwargs

    def get_module(self, process_args):
        """
        Instantiates and returns the indicated module.

        Parameters
        ==========
        process_args
            A function to call on arguments to have modules replaced.

        Return Value
        ============
        The value obtained when the constructor of the indicated class is
        called with the indicated positional arguments and keyword arguments.
        """
        return self.class_(*process_args(self.args),
                           **process_args(self.kwargs))

    def __repr__(self):
        return "module<inst>({}, {}, {})".format(repr(self.class_),
                                                 repr(self.args),
                                                 repr(self.kwargs))


class ModuleReference(BaseModule):
    """
    Represents a reference to an existing, already-instantiated module.
    """
    def __init__(self, name):
        """
        Parameters
        ==========
        name
            The name of the referenced module.
        """
        self.name = name

    def get_module(self, process_args):
        """
        Returns the name of the module referenced.

        Parameters
        ==========
        process_args
            Ignored.

        Return Value
        ============
        The name of the module referenced.
        """
        return self.name

    def __repr__(self):
        return "module<reference>({})".format(repr(self.name))


def module(*args, **kwargs):
    """
    Based on the arguments, instantiates either a ModulReference or
    InstantiableModulea nd returns it. Raises an AssertionError if the
    arguments are not suitable for either class.

    Only positional or keyword arguments may be passed, not a mixture of the
    two.
    """
    assert (len(args) == 0 or len(kwargs) == 0) and \
           (len(args) > 0 or len(kwargs) > 0)
    if len(args) > 0:
        assert len(args) == 1 or len(args) == 3
        reference = len(args) == 1
    else:
        keys = set(kwargs.keys())
        assert keys == {"name"} or ("class_" in kwargs and
                                    len(keys - {"class_", "args", "kwargs"}) ==
                                    0)
        reference = "name" in kwargs
    if reference:
        return ModuleReference(*args, **kwargs)
    return InstantiableModule(*args, **kwargs)
