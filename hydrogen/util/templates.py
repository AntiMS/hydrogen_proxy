import os

from hydrogen.conf import settings

from jinja2 import Environment, PackageLoader, FileSystemLoader, ChoiceLoader


__all__ = ['template_environment']


def guess_autoescape(template_name):
    """
    This function is used by the Jinja2 "autoescape" extension to determine
    whether a given template is to be autoescaped. This code is taken directly
    from http://jinja.pocoo.org/docs/dev/api/#autoescaping .

    Parameters
    ==========
    template_name
        The name of the template for which to determine if autoescaping is to
        be enabled.
    """
    if template_name is None or '.' not in template_name:
        return False
    ext = template_name.rsplit('.', 1)[1]
    return ext in ('html', 'htm', 'xml')


def create_template_environment():
    """
    Creates a Jinja template environment.
    """
    template_dirs = getattr(settings, "TEMPLATE_DIRS", (os.path.join(
            settings.SETTINGS_DIR, "templates"),))
    loaders = [FileSystemLoader(dir) for dir in template_dirs]
    loaders.append(PackageLoader("hydrogen"))
    return Environment(loader=ChoiceLoader(loaders),
                       autoescape=guess_autoescape,
                       extensions=['jinja2.ext.with_', 'jinja2.ext.autoescape']
                       )


class LazyTemplateEnvironment:
    """
    Takes the place of a Jinja template environment. Loads the actual Jinja
    template environment under-the-hood the first time an attribute is
    requested. All attribute accesses are passed through to the underlying
    wrapped template environment.
    """
    def __init__(self):
        self._wrapped = None

    def __getattr__(self, name):
        if self._wrapped is None:
            self._wrapped = create_template_environment()
        return getattr(self._wrapped, name)


# A wrapper for a lazily-loaded Jinja template environment for use by Hydrogen
# modules.
template_environment = LazyTemplateEnvironment()
