import subprocess
from queue import Queue, Empty
from threading import Thread

from hydrogen.conf import settings
from hydrogen.message import Body


class OutputQueuer:
    def __init__(self):
        self.stopped = False

    def stop(self):
        self.stopped = True

    def __call__(self, out, queue):
        while not self.stopped:
            chunk = out.read(settings.CHUNK_SIZE)
            if len(chunk) == 0:
                break
            queue.put(chunk)

        queue.put(b'')
        out.close()


class SubprocessBody(Body):
    def __init__(self, command, input):
        self.proc = subprocess.Popen(command, bufsize=1, stdin=subprocess.PIPE,
                                     stdout=subprocess.PIPE)
        self.queue = Queue()
        self.queuer = OutputQueuer()
        self.thread = Thread(target=self.queuer, args=(self.proc.stdout,
                                                       self.queue))
        self.thread.daemon = True
        self.thread.start()
        self.input = input
        super().__init__()

    def _get_from_stream(self, s):
        while True:
            try:
                chunk = self.queue.get(timeout=.1)
            except Empty:
                if self.proc.poll() is not None:
                    return b''
                c = self.input.read(settings.CHUNK_SIZE)
                if len(c) > 0:
                    self.proc.stdin.write(c)
                else:
                    self.proc.stdin.close()
            else:
                if len(chunk) == 0:
                    self.input.close()
                    self.proc.stdin.close()
                    self.proc.stdout.close()
                    self.queuer.stop()
                return chunk

    def close(self):
        super().close()
        self.proc.kill()
        self.proc.wait()


class SubprocessModifyingHandler:
    """
    A handler which when instantiated takes a command-line filter command and
    when handling a response, runs the entire response through that filter.  It
    returns a modified HTTPResponse whose body is the output of the command.
    """
    def __init__(self, child, command):
        """
        Command must be an iterable of strings.  The first of these strings is
        the command and the rest are arguments to the command.
        """
        self.child = child
        self.command = command

    def __call__(self, request):
        response = self.child(request)

        headers = response.headers
        del headers['Content-Length']

        body = SubprocessBody(self.command, response.get_input(head=False))
        out = response.clone(body=body)

        return out
