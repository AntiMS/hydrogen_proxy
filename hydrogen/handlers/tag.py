class Tagger:
    """
    Base class for tagging handlers.
    """
    def __init__(self, child, *tags):
        """
        Parameters
        ==========
        child
            The child request handler to which to pass the request.

        *tags
            The tags (strings) to apply.
        """
        self.child = child
        self.tags = tags


class RequestTaggingHandler(Tagger):
    """
    A handler which tags requests.
    """
    def __call__(self, request):
        request.tags.update(self.tags)
        return self.child(request)


class ResponseTaggingHandler(Tagger):
    """
    A handler which tags responses.
    """
    def __call__(self, request):
        response = self.child(request)
        response.tags.update(self.tags)
        return response
