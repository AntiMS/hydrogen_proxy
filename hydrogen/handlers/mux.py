class MultiplexingHandler:
    """
    A handler which wraps multiple other handlers and chooses to which wrapped
    handler to send a given message based on a series of matchers.
    """
    def __init__(self, default, *args):
        """
        Parameters
        ==========
        default
            The default handler to which to send the request if none of the
            matchers match.

        *args
            Iterables of length 2.  The first item of each iterable must be a
            matcher instance and the second a wrapped handler.  The
            MuxingHandler passes any given request the first wrapped handler
            whose associated matcher matches the request.
        """
        self.tuples = args
        self.default = default

    def __call__(self, request):
        for matcher, handler in self.tuples:
            if matcher.matches(request):
                return handler(request)
        return self.default(request)
