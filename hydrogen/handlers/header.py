class HeaderRemover:
    """
    A mixin which removes a list of headers from a given transaction.
    Subclasses must call the process() method to perform the removals.
    """
    def __init__(self, child, *args):
        """
        Parameters
        ==========
        child
            The child handler to which to pass the request.

        *args
            Names of headers to remove as strings.
        """
        self._child = child
        self._to_remove = args

    def process(self, transaction):
        """
        Performs the removal of headers.

        Parameters
        ==========
        transaction
            The transaction (request or response) from which to remove headers.
        """
        for h in self._to_remove:
            try:
                del transaction.headers[h]
            except Exception:
                pass


class HeaderAdder:
    """
    A mixin which adds a given set of headers. Subclasses must call the
    process() method to perform add operations.
    """
    def __init__(self, child, headers):
        """
        Parameters
        ==========
        child
            The child handlerto which to pass the request.

        headers
            A dictionary or iterable of two-tuplies of headers to set.
        """
        self._child = child
        self._headers = headers.items() if hasattr(headers, "items") else \
            headers

    def process(self, transaction):
        """
        Performs the add operation.

        Parameters
        ==========
        transaction
            The transaction (request or response) on which to add headers.
        """
        for h, v in self._headers:
            transaction.headers[h] = v


class HeaderSetter(HeaderAdder):
    """
    A mixin which sets a given set of headers to given values (consolidating
    duplicate header keys if necessary). Subclasses must call the process()
    method to perform set operations.
    """
    def process(self, transaction):
        """
        Performs the set operation.

        Parameters
        ==========
        transaction
            The transaction (request or response) on which to set headers.
        """
        for h, v in self._headers:
            del transaction.headers[h]
        super().process(transaction)


class RequestProcessor:
    """
    A handler which delegates an operation on a request to the "process()"
    method of subclasses.
    """
    def __call__(self, request):
        self.process(request)
        return self._child(request)


class ResponseProcessor:
    """
    A handler which delegates an operation on a response to the "process()"
    method of subclasses.
    """
    def __call__(self, request):
        response = self._child(request)
        self.process(response)
        return response


class RequestHeaderRemovingHandler(RequestProcessor, HeaderRemover):
    """
    A handler which removes a given set of request headers before sending the
    request on to the child handler.
    """
    pass


class RequestHeaderAddingHandler(RequestProcessor, HeaderAdder):
    """
    A handler which adds a given set of request headers before sending the
    request on to the child handler.
    """
    pass


class RequestHeaderSettingHandler(RequestProcessor, HeaderSetter):
    """
    A handler which sets a given set of request headers to given values before
    sending the request on to the child handler.
    """
    pass


class ResponseHeaderRemovingHandler(ResponseProcessor, HeaderRemover):
    """
    A handler which removes a given set of response headers from the response
    returned by the child handler.
    """
    pass


class ResponseHeaderAddingHandler(ResponseProcessor, HeaderAdder):
    """
    A handler which adds a given set of response headers to the response
    returned by the child handler.
    """
    pass


class ResponseHeaderSettingHandler(ResponseProcessor, HeaderSetter):
    """
    A handler which sets a given set of response headers to given values on the
    response returned by the child handler.
    """
    pass
