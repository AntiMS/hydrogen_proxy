import html.parser

from .tree import (SelfClosingTagNode, TagNode, CharrefNode, EntityrefNode,
                   DataNode, CommentNode, DeclNode, PiNode)


# "Void" tags. Tags which should be treated as self-closing.
_void = ("area", "base", "basefont", "br", "hr", "input", "img", "link",
         "meta", "col", "command", "embed", "keygen", "param", "source",
         "track", "wbr", "bgsound", "frame", "spacer", "isindex", "menuitem",)

# Tags for which the closing tag is sometimes omitted.
_noclose = ("li",)


class HydrogenHTMLParser(html.parser.HTMLParser):
    """
    An iterative HTML parser which builds an HTML tree object and calls an
    event handler on each event (such as a start tag, end tag, or comment.)
    """
    def __init__(self, tree, callback=None, *args, **kwargs):
        """
        Parameters
        ==========
        tree
            The root node of the tree to build.

        callback
            Optional. A callback to call for each event. Takes three arguments
            -- an event name as a string, the node it created or affected, and
            the tree.

        *args, **kwargs
            All remaining positional and keyword arguments are passed to the
            structor of html.parser.HTMLParser .
        """
        super().__init__(*args, **kwargs)
        self.tree = tree
        self.current_node = self.tree

        def nullcb(event, node):
            pass

        self._cb = callback or nullcb

    def handle_startendtag(self, tag, attrs):
        node = SelfClosingTagNode(tag, attrs)
        self.current_node.add_child(node)
        self._cb("starttag", node)
        self._cb("startendtag", node)
        self._cb("endtag", node)

    def handle_starttag(self, tag, attrs):
        if tag in _void:
            self.handle_startendtag(tag, attrs)
            return
        if tag in _noclose:
            if tag == self.current_node.tagname:
                self.handle_endtag(tag)
        node = TagNode(tag, attrs)
        self.current_node.add_child(node)
        self.current_node = node
        self._cb("starttag", node)

    def handle_endtag(self, tag):
        node = self.current_node
        while node.parent is not None and tag != node.tagname:
            node = node.parent
        if node != self.tree:
            new_cn = node.parent
            cn = self.current_node
            while cn != new_cn:
                cn.close()
                n = cn.parent
                self._cb("endtag", cn)
                cn = n
            self.current_node = new_cn

    def handle_charref(self, name):
        node = CharrefNode(name)
        self.current_node.add_child(node)
        self._cb("charref", node)

    def handle_entityref(self, name):
        node = EntityrefNode(name)
        self.current_node.add_child(node)
        self._cb("entityref", node)

    def handle_data(self, data):
        node = DataNode(data)
        self.current_node.add_child(node)
        self._cb("data", node)

    def handle_comment(self, comment):
        node = CommentNode(comment)
        self.current_node.add_child(node)
        self._cb("comment", node)

    def handle_decl(self, decl):
        node = DeclNode(decl)
        self.current_node.add_child(node)
        self._cb("decl", node)

    def handle_pi(self, data):
        node = PiNode(data)
        self.current_node.add_child(node)
        self._cb("pi", node)
