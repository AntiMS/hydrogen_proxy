from hydrogen.conf import settings
from hydrogen.message.body.base import Body


class HTMLModifyingBody(Body):
    """
    A Body which parses the given input's contents (typically a response body)
    iteratively as HTML using the given HydrogenHTMLParser.

    The HTMLModifyingBody also serializes the tree back to HTML (again
    iteratively) and outputs it.

    Only part of the tree is in memory at any one time. That is, earlier
    portions of the tree may already have been serialized.
    """
    def __init__(self, input, tree, parser):
        """
        Parameters
        ==========
        input
            The input from which to read the HTML document to be parsed.

        tree
            The (root node of the) tree to build.

        parser
            The HTMLParser to which to feed the input.
        """
        self._input = input
        self._tree = tree
        self._parser = parser
        self._next_node = self._tree
        super().__init__()

    def _move_to_next_node(self):
        """
        Serializes the next node and returns it as a string. Also updates the
        self._next_node variable to point at the next node to be serialized.
        """
        if len(getattr(self._next_node, "children", [])) > 0:
            out = self._next_node.start_to_string()
            self._next_node.serialized = True
            self._next_node = self._next_node.children[0]
            return out

        out = self._next_node.to_string()
        nn = self._next_node.next_sibling()
        while nn is None:
            n = self._next_node
            self._next_node = self._next_node.parent
            if self._next_node is None:
                return out
            n.remove()
            out += self._next_node.end_to_string()
            nn = self._next_node.next_sibling()
        self._next_node.remove()
        self._next_node = nn

        return out

    def _has_more_to_serialize(self):
        """
        Returns True if the tree contains at least one node ready to be
        serialized.
        """
        return self._tree.complete or self._tree.closed_node_count >= \
            settings.MAX_NODE_COUNT

    def _get_from_stream(self, s):
        while self._tree.closed_node_count < settings.MAX_NODE_COUNT and not \
                self._tree.complete:
            chunk = self._input.read(settings.CHUNK_SIZE)
            if len(chunk) == 0:
                self._tree.close_all()
                break
            else:
                self._parser.feed(chunk.decode("UTF-8", "ignore"))

        out = ""
        while self._has_more_to_serialize():
            out += self._move_to_next_node()

        return out.encode("UTF-8")

    def close(self):
        super().close()
        self._tree.close_all()
        self._input.close()
