import html


class HTMLNode:
    """
    Superclass for HTML node objects. Each HTML node object has a parent and a
    root node (which must be an ancestor.) This superclass provides several
    convenience methods for retrieving siblings, determining the index of thi
    node among its siblings, and removing this node from the tree, among
    others.
    """
    def __init__(self):
        self._parent = None
        self.root = None

    @property
    def parent(self):
        return self._parent

    @property
    def position(self):
        """
        Returns the index of this node from among its siblings. The first child
        node of any node will have the index 0 and the second will have the
        index 1 etc.
        """
        return self.parent.children.index(self)

    def _sibling(self, pos):
        if self.parent is None:
            return None
        pos += self.position
        if pos >= len(self.parent.children) or pos < 0:
            return None
        return self.parent.children[pos]

    def next_sibling(self):
        """
        Returns the sibling of this node directly following this node or None
        if no such node exists..
        """
        return self._sibling(1)

    def prev_sibling(self):
        """
        Returns the sibling of this node directly preceeding this node or None
        if no such node exists.
        """
        return self._sibling(-1)

    def remove(self):
        """
        Removes this node from the tree.
        """
        self.parent.children.remove(self)
        self.root.node_removed(self)
        self.root = None
        self._parent = None
        return self


class ParentNode(HTMLNode):
    """
    A superclass for nodes which contain other nodes as children.
    """
    def __init__(self):
        super().__init__()
        self.children = []
        self.root = self

    def add_child(self, node, position=None):
        """
        Makes a given node the child of this node.

        Parameters
        ==========
        node
            The node to make a child of this node.

        position
            Optional. The position in the list of children. Defaults to the
            last available position.
        """
        if position is None:
            position = len(self.children)
        node.root = self.root
        node._parent = self
        self.children.insert(position, node)
        self.root.node_added(node)
        return node

    def to_string(self):
        """
        Serializes the children of this node to a single string.
        """
        out = ""
        for t in self.children:
            out += t.to_string()
        return out


class RootNode(ParentNode):
    """
    The root node of a tree. Keeps track of a count of nodes in the tree.
    """
    def __init__(self):
        super().__init__()
        self.node_count = 0
        self.closed_node_count = 0

    @property
    def complete(self):
        """
        Returns true if every opened node has been closed.
        """
        return self.node_count != 0 and self.node_count == \
            self.closed_node_count

    def _count_nodes(self, node):
        nodes, closed_nodes = (1, 1 if getattr(node, "closed", True) else 0)

        if hasattr(node, "children"):
            for c in node.children:
                n, cn = self._count_nodes(c)
                nodes += n
                closed_nodes += cn

        return nodes, closed_nodes

    def node_removed(self, node):
        """
        Called when a node is removed from the tree.

        Parameters
        ==========
        node
            The node removed.
        """
        n, cn = self._count_nodes(node)
        self.node_count -= n
        self.closed_node_count -= cn

    def node_added(self, node):
        """
        Called when a node is added to the tree.

        Parameters
        ==========
        node
            The node added.
        """
        n, cn = self._count_nodes(node)
        self.node_count += n
        self.closed_node_count += cn

    def node_closed(self, node):
        """
        Called when a node is closed.

        Parameters
        ==========
        node
            The node closed.
        """
        self.closed_node_count += 1

    def start_to_string(self):
        """
        This method is only exists to make the root node's API match that of a
        TagNode for convenience. Always returns an empty string.
        """
        return ""

    def children_to_string(self):
        """
        Serializes all children of this node to a single string.
        """
        return super().to_string()

    def end_to_string(self):
        """
        This method is only exists to make the root node's API match that of a
        TagNode for convenience. Always returns an empty string.
        """
        return ""

    def _close_all(self, node):
        if hasattr(node, "children"):
            for c in node.children:
                if hasattr(c, "close"):
                    c.close()
                self._close_all(c)

    def close_all(self):
        """
        Closes all open nodes in this tree.
        """
        self._close_all(self)


class TagMixin:
    """
    A convenience mixin for all node classes which have a tagname and
    attributes. This includes start tags with corresponding end tags and
    self-closing tags.
    """
    def __init__(self, tagname, attrs):
        self.tagname = tagname
        self.attrs = dict(attrs)

    def tagstart_to_string(self):
        """
        A convenience method which outputs the beginning of the tag as a
        string.
        """
        out = "<" + self.tagname
        for k, v in self.attrs.items():
            if v is None:
                v = k
            out += " " + k + "=\"" + html.escape(v, True) + "\""
        return out


class TagNode(ParentNode, TagMixin):
    """
    Represents a start tag/end tag pair.
    """
    def __init__(self, tagname, attrs, closed=False):
        """
        Parameters
        ==========
        tagname
            The name of the tag.

        attrs
            A dictionary of attributes on this tag.

        closed
            Optional. Indicates whether this tag is already closed at the time
            of instantiation. Defaults to False.
        """
        ParentNode.__init__(self)
        TagMixin.__init__(self, tagname, attrs)
        self._closed = closed
        self.serialized = False

    @property
    def closed(self):
        """
        Returns true if this tag is closed.
        """
        return self._closed

    def close(self):
        """
        Closes this node.
        """
        if not self._closed:
            self._closed = True
            self.root.node_closed(self)
        return self

    def start_to_string(self):
        """
        Serializes the start tag of this tag node.
        """
        return self.tagstart_to_string() + ">"

    def children_to_string(self):
        """
        Serializes the children of this tag as a single string.
        """
        return super().to_string()

    def end_to_string(self):
        """
        Serializes the end tag of this tag node.
        """
        return "</" + self.tagname + ">"

    def to_string(self):
        """
        Serializes this entire tag, including child tags, to a single string.
        """
        return self.start_to_string() + self.children_to_string() + \
            self.end_to_string()


class SelfClosingTagNode(HTMLNode, TagMixin):
    """
    Represents a self-closing tag.
    """
    def __init__(self, tagname, attrs):
        """
        Parameters
        ==========
        tagname
            The name of the tag.

        attrs
            A dictionary of attributes on this tag.
        """
        HTMLNode.__init__(self)
        TagMixin.__init__(self, tagname, attrs)

    def to_string(self):
        """
        Serializes this tag to a string.
        """
        return self.tagstart_to_string() + "/>"


class CharrefNode(HTMLNode):
    """
    Represents a numerial HTML character reference.
    """
    def __init__(self, name):
        """
        Parameters
        ==========
        name
            The name (number) of this character reference.
        """
        self.name = name

    def to_string(self):
        """
        Serializes this node to a string.
        """
        return "&#" + self.name + ";"


class EntityrefNode(HTMLNode):
    """
    Represents a named HTML character reference.
    """
    def __init__(self, name):
        """
        Parameters
        ==========
        name
            The name of this character reference.
        """
        self.name = name

    def to_string(self):
        """
        Serializes this node to a string.
        """
        return "&" + self.name + ";"


class DataNode(HTMLNode):
    """
    Represents a text node.
    """
    def __init__(self, data):
        """
        Parameters
        ==========
        data
            The text of the text node.
        """
        self.data = data

    def to_string(self):
        """
        Serializes this node to a string.
        """
        return html.escape(self.data)


class CommentNode(HTMLNode):
    """
    Represents a comment node.
    """
    def __init__(self, comment):
        """
        Parameters
        ==========
        comment
            The text of the comment.
        """
        self.comment = comment

    def to_string(self):
        """
        Serializes this node to a string.
        """
        return "<!--" + self.comment + "-->"


class DeclNode(HTMLNode):
    """
    Represents a declaration node.
    """
    def __init__(self, decl):
        """
        Parameters
        ==========
        decl
            The body of the declaration.
        """
        self.decl = decl

    def to_string(self):
        """
        Serializes this node to a string.
        """
        return "<!" + self.decl + ">"


class PiNode(HTMLNode):
    """
    Represents a processing instruction node.
    """
    def __init__(self, data):
        """
        Parameters
        ==========
        data
            The body of the processing instruction.
        """
        self.data = data

    def to_string(self):
        """
        Serializes this node to a string.
        """
        return "<?" + self.data + ">"
