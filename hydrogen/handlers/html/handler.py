from .parser import HydrogenHTMLParser
from .body import HTMLModifyingBody
from .tree import RootNode


# The response content types which may be parsed as HTML.
_contenttypes = ("text/html", "text/xhtml", "text/xhtml+xml", "text/xml+xhtml",
                 "application/html", "application/xhtml",
                 "application/xhtml+xml", "application/xml+xhtml",)


class HTMLModifyingHandler:
    """
    A handler which iteratively parses the response body from the underlying
    handler as HTML using the HydrogenHTMLParser. Allows for callbacks which
    may modify the tree in transit.

    These callbacks, called "modifiers", must not assume that the entire tree
    is currently available to be modified all at once since the tree is parsed
    iteratively. Earlier portions of the tree may already be serialized and
    returned to the client and later portions have yet to be parsed.

    The number of nodes to be kept in memory at one time can be controlled via
    the stting "MAX_NODE_COUNT".

    Modifiers are functions which take three arguments. The first is the name
    of the event which occurred such as "starttag" for the start of a tag being
    parsed, "endtag" for an end tag being parsed, or "data" for text. The
    second is the node object parsed. The third is a reference to the root node
    of the tree.
    """
    def __init__(self, child, *args):
        """
        Parameters
        ==========
        child
            The child handler to which to forward requests and from which to
            receive responses.

        *args
            Modifiers to apply to the tree.
        """
        self.child = child
        self._mods = args

    def __call__(self, request):
        response = self.child(request)

        content_type = response.headers['Content-Type']
        if content_type is None:
            return response
        content_type = content_type.split(";")[0]
        if content_type not in _contenttypes:
            return response

        tree = RootNode()
        new_response = None

        def cb_func(event, node):
            for m in self._mods:
                m(event, node, tree, new_response)

        input = response.get_input(head=False)
        parser = HydrogenHTMLParser(tree, cb_func)
        body = HTMLModifyingBody(input, tree, parser)

        del response.headers['Content-Length']

        new_response = response.clone(body=body)

        return new_response
