import gzip


class GunzippingHandler:
    """
    A handler which converts "Content-Encoding: gzip" HTTPResponses to
    HTTPResponses with no Content-Encoding.  This simplifies other handlers so
    that they don't need to unzip the content themselves.
    """
    def __init__(self, child):
        super().__init__()
        self.child = child

    def _del(self, d, k):
        try:
            del d[k]
        except Exception:
            pass

    def __call__(self, request):
        response = self.child(request)
        if response.headers.get('Content-Encoding', None) != "gzip":
            return response

        self._del(response.headers, 'Content-Length')

        input = response.get_input(head=False)
        gzipfile = gzip.GzipFile(fileobj=input, mode='rb')
        response = response.clone(body=None, content=gzipfile)
        self._del(response.headers, 'Content-Encoding')
        if response.length is not None:
            response.headers['Content-Length'] = str(response.length)
        else:
            self._del(response.headers, 'Connection')
            response.headers['Connection'] = "Close"
            self._del(response.headers, 'Content-Length')
        return response
