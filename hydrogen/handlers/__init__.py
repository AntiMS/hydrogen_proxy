from .mux import MultiplexingHandler as mux
from .gunzip import GunzippingHandler as gunzip
from .submod import SubprocessModifyingHandler as submod
from .header import (RequestHeaderRemovingHandler as rmreqhdr,
                     RequestHeaderAddingHandler as addreqhdr,
                     RequestHeaderSettingHandler as setreqhdr,
                     ResponseHeaderRemovingHandler as rmreshdr,
                     ResponseHeaderAddingHandler as addreshdr,
                     ResponseHeaderSettingHandler as setreshdr)
from .html.handler import HTMLModifyingHandler as htmlmod
from .tag import (RequestTaggingHandler as tagreq,
                  ResponseTaggingHandler as tagres)


__all__ = ['mux', 'gunzip', 'submod', 'htmlmod', 'rmreqhdr', 'addreqhdr',
           'setreqhdr', 'rmreshdr', 'addreshdr', 'setreshdr', 'tagreq',
           'tagres']
