Hydrogen Proxy is (planned to be) a highly flexible, extensible, and scriptable
HTTP proxy written in Python version 3 and intended for those who want more
control over their web browsing. Hydrogen Proxy is currently beta-quality
software.

Uses
====
Among other uses, Hydrogen Proxy fills some of the same use cases as
Greasemonkey/Scriptish but does its work before the browser receives the page
rather than after the page has started rendering. In many ways, this makes for
much less complexity in user scripts.

Another benefit Hydrogen Proxy has over options such as Greasemonkey and
Scriptish is that Hydrogen Proxy can also modify the request in ways
Greasemonkey and Scriptish cannot. Also, Hydrogen Proxy is browser/client
agnostic.

Hydrogen Proxy also has the ability to route requests very powerfully, for
instance routing requests for a certian list of domains through one upstream
proxy and all other requests via direct TCP connections. Hydrogen Proxy can also
selectively simulate end servers and return contents of files found on the file
system or dynamically-generated web application content rather than actually
sending requests to any particular server. This is useful, for instance, if you
wish to replace a particular website's CSS or JavaScript with your own or if a
module needs to present an interface to the user by which they may modify
settings of modules initialized by Hydrogen Proxy at runtime.

Hydrogen Proxy is also massively extensible with simple API's to be able to do
just about anything an HTTP proxy could conceivably do.

Running Tests
=============
The unit tests can be run using the following command from the root of the
repository::

    > python3 -m unittest

Using
=====

Installing
----------
First, install Hydrogen Proxy with the following commands::

    > ./setup.py build
    > ./setup.py install
    > mkdir -pf ~/.hydrogen_proxy/certs

Next, you'll need to copy the example settings.py to ~/.hydrogen_proxy and edit
it according to your needs::

    > mkdir -f ~/.hydrogen_proxy
    > cp settings.py.example ~/.hydrogen_proxy/settings.py
    > vim ~/.hydrogen_proxy/settings.py

Accessing HTTPS sites through Hydrogen Proxy will require generating a root
certificate and installing it in each client's trusted root CA store. Running
the following command will require that you have OpenSSL installed::

    > h2 mkcert

This will place the resulting root certificate at the path
~/.hydrogen_proxy/ca.pem and a fake private key to use for each individual host
at ~/.hydrogen_proxy/ckey.pem .

One more detail. Hydrogen Proxy needs a directory in which to store
certificates created on-the-fly.

    > mkdir ~/.hydrogen_proxy/certs

Now, you're ready to run Hydrogen Proxy::

    > h2 proxy

Configuring Your HTTP Client
----------------------------
You'll need to do two things to make your client work properly with Hydrogen
Proxy. The first is to configure the client to use a proxy listening on Hydrogen
Proxy's configured host and port, and the second is to install Hydrogen Proxy's
generated self-signed certificate in your client's trusted certificate authority
store. The latter is necessary so that Hydrogen Proxy will be able to examine
the contents of HTTPS requests without the browser throwing any warnings. (Don't
worry. Hydrogen Proxy validates the server's *real* certificate and throws an
error if the server's certificate does not validate.)

To determine on what interface and port Hydrogen Proxy will be listening,
please refer to the configuration. Its location by default is at
~/.hydrogen_proxy/settings.py .

Hydrogen Proxy's root certificate is located at ~/.hydrogen_proxy/ca.pem .

To determine how to configure your particular client, please refer to its
documentation.

Extending Hydrogen Proxy
------------------------
Any handlers, matchers, or other modules you might wish to write for use with
Hydrogen Proxy can be placed in the directory ~/.hydrogen_proxy . Hydrogen Proxy
automatically adds this directory to the PYTHONPATH on startup, so any module in
that directory may be referenced easily in your settings.py .

If you write something good and generally useful to many users of Hydrogen
Proxy, pull requests are much appreciated!

Re-Generating the Self-Signed Certificate
-----------------------------------------
If you ever need to re-generate Hydrogen Proxy's self-signed certificate,
you'll need to remove the previous certificate from your client's trusted
store, and install the new one. You'll also need to clean up the "fake" server
certificates Hydrogen Proxy has generated previously. This can be done with the
following commands::

    > h2 cleancerts
    > rm ~/.hydrogen_proxy/certs/*
    > rm ~/.hydrogen_proxy/ca.pem ~/.hydrogen_proxy/ckey.pem
    > h2 mkcert

After performing these steps, you will most likely need to install the
newly-generated root certificate in your client again.

Legal Stuff
===========
Copyright 2014 Richard Anderson

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received  a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
