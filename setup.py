#!/usr/bin/env python3

from distutils.core import setup


setup(
    name="Hydrogen Proxy",
    version="0.1a",
    description="Hydrogen Proxy is (planned to be) a highly flexible, "
                "extensible, and scriptable HTTP proxy written in Python "
                "version 3 and intended for those who want more control over "
                "their web browsing.",
    author="AntiMS",
    author_email="antims923@gmail.com",
    url="http://github.com/AntiMS/hydrogen_proxy",
    packages=['hydrogen', 'hydrogen.modules', 'hydrogen.modules.handlers',
              'hydrogen.modules.handlers.html', 'hydrogen.modules.matchers',
              'hydrogen.message', 'hydrogen.message.body',
              'hydrogen.modules.receivers', 'hydrogen.modules.senders'],
    scripts=['h2'],
    data_files=[('share/hydrogen_proxy', ['settings.py.example', 'README.rst'])
                ],
    install_requires=['pyOpenSSL>=0.14', 'Jinja2>=2.8'],
)
