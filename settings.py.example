from hydrogen.helpers import module


def reddit_remove_sidebar(event, node, tree, response):
    if event == "endtag" and node.attrs.get("class", "") == "side":
        node.remove()

CHAINS = {
    "http": "hydrogen.senders.http",
    # The following examples are all equivalent to the above line:
    # "http": ("hydrogen.senders.http",),
    # "http": ("hydrogen.senders.http", ()),
    # "http": ("hydrogen.senders.http", (), {}),
    # "http": {"class": "hydrogen.senders.http"}
    # "http": {"class": "hydrogen.senders.http", "args": ()}
    # "http": {"class": "hydrogen.senders.http", "kwargs": {}}
    # "http": {"class": "hydrogen.senders.http", "args": (), "kwargs": {}}
    # "http": module("hydrogen.senders.http", (), {}),
    # "http": module(class_="hydrogen.senders.http", args=(), kwargs={}),

    "gunzip": {
        "class": "hydrogen.handlers.gunzip",
        "args": (module("http"),) # module("http") automatically resolves to
                                  # the "http"object defined above. It is
                                  # passed as the first positional argument to
                                  # this "gunzip" class' constructor.
    },

    "out": module("gunzip"), # You can also alias modules like this.

    "google_submod": {
        "class": "hydrogen.handlers.submod",
        "args": (module("out"), ("sed", "-e", "s/Google/Evil/g")),
    },
    "reddit_htmlmod": {
        "class": "hydrogen.handlers.htmlmod",
        "args": (module("out"), reddit_remove_sidebar)
    },
    "admin": "hydrogen.contrib.admin",

    "mux": {
        "class": "hydrogen.handlers.mux",
        "args": (
            module("out"),
            (
                # In the following form, "module" causes an anonymous module to
                # be instantiated on the fly. In this case, a domain matcher.
                module("hydrogen.matchers.domain", (r"^www.google.com$",), {}),
                module("google_submod")
            ),
            (
                module("hydrogen.matchers.url", (r"^http://www.reddit.com/$",), {}),
                module("reddit_htmlmod")
            ),
            (
                module("hydrogen.matchers.domain", (r"^hydrogen\.proxy$",), {}),
                module("admin")
            ),
        )
    },

    "proxy": {
        "class": "hydrogen.receivers.proxy",
	"args": (module("mux"),),
        "kwargs": {
            "parallel_method": "thread",
            "listen_address": ("127.0.0.1", 8080)
        }
    }
}

# These values control how much to hold in memory at a time.  Smaller values
# are better for systems with limited memory.
CHUNK_SIZE = 65536
MAX_NODE_COUNT = 2048

# True will cause Hydrogen Proxy to output some information useful for
# debugging to STDOUT.
VERBOSE = False
