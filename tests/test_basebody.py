from .base import BaseTestCase


class TestBaseBody(BaseTestCase):
    """
    Tests the Body class.
    """
    def setUp(self):
        """
        Imports the Body class.
        """
        super().setUp({"CHUNK_SIZE": 16})
        global Body
        from hydrogen.message import Body

    def test_all(self):
        class MockBody(Body):
            def __init__(self, content, length=None):
                self.content = content
                super(MockBody, self).__init__(length)

            def _get_from_stream(self, n):
                out = self.content[:n]
                self.content = self.content[n:]
                return out

        body = MockBody(b"a" * 32, 32)
        self.assertEqual(body.length, 32)
        self.assertEqual(body.read(128), b"a" * 32)
        self.assertEqual(body.length, 32)

        body = MockBody(b"a" * 32)
        self.assertIsNone(body.length)
        self.assertEqual(body.read(128), b"a" * 32)
        self.assertEqual(body.length, 32)

        body = MockBody(b"a" * 8)
        self.assertEqual(body.length, 8)
        self.assertEqual(body.read(128), b"a" * 8)
        self.assertEqual(body.length, 8)
