from http.client import parse_headers
from io import BytesIO

from .base import BaseTestCase


_EXPECTED_BODY = b'<!DOCTYPE html><html><head><title>Hydrogen Proxy - Error' \
                 b'</title></head><body><h1>Hydrogen Proxy - Error</h1><p>' \
                 b'Hydrogen Proxy encountered an error while trying to ' \
                 b'service a request. Error details follow:</p><p><pre>' \
                 b'<code>NoneType: None</code></pre></p></body></html>'


class TestError(BaseTestCase):
    """
    Tests the error page response creator.
    """
    def setUp(self):
        """
        Imports the error_response function to be tested and supporting things.
        """
        super().setUp({"TEMPLATE_DIRS": ()})
        global error_response, HTTPRequest
        from hydrogen.error import error_response
        from hydrogen.message import HTTPRequest

    def test_error(self):
        """
        Tests the error page response creator.
        """
        req = HTTPRequest(("127.0.0.1", 80), "GET", "http://example.com/",
                          "HTTP/1.1", parse_headers(BytesIO(b"\r\n")),
                          BytesIO(b""))
        err = Exception("Danger Will Robinson.")
        res = error_response(req, err)
        inp = res.get_input(head=False)
        body = b""

        while True:
            chunk = inp.read(16)
            if chunk == b"":
                break
            body += chunk
        bodylen = len(body)

        # Remove all unnecessary whitespace to ease testing.
        prev_len = len(body) + 1
        while len(body) < prev_len:
            prev_len = len(body)
            body = body.replace(b"\n    ", b"\n")
        body = body.replace(b"\n", b"")

        self.assertEqual(res.status, 500)
        self.assertEqual(res.message, "Internal Server Error")
        self.assertEqual(res.http_version, "HTTP/1.1")
        self.assertEqual(len(res.headers), 3)
        self.assertEqual(res.headers["Server"], "Hydrogen Proxy")
        self.assertEqual(res.headers["Content-Type"], "text/html")
        self.assertEqual(res.headers["Content-Length"], str(bodylen))
        self.assertEqual(body, _EXPECTED_BODY)
