import io

from .base import BaseTestCase


class TestResponse(BaseTestCase):
    """
    Tests the HTTPResponse class.
    """
    def setUp(self):
        """
        Imports the HTTPResponse class.
        """
        super().setUp({"CHUNK_SIZE": 64})
        global HTTPResponse
        from hydrogen.message import HTTPResponse

    def test_properties(self):
        """
        Tests the properties of the HTTPResponse class.
        """
        res = HTTPResponse(True, "200", "Ok", "HTTP/1.1",
                           {"Argyle-Fishsticks": "Procrastinating"},
                           content=io.BytesIO(b"Testing."))
        self.assertEqual(res.request, True)
        self.assertEqual(res.status, "200")
        self.assertEqual(res.message, "Ok")

    def test_get_body(self):
        """
        Tests the get_body() method of the HTTPResponse class.
        """
        # Zero-length.
        res = HTTPResponse(True, "200", "Ok", "HTTP/1.1",
                           {"Argyle-Fishsticks": "Procrastinating",
                            "Content-Length": "0"},
                           content=io.BytesIO(b""))
        self.assertEqual(res.body.length, 0)
        self.assertEqual(res.body.read(10), b"")
        self.assertEqual(res.body.length, 0)

        # Simple
        res = HTTPResponse(True, "200", "Ok", "HTTP/1.1",
                           {"Argyle-Fishsticks": "Procrastinating"},
                           content=io.BytesIO(b"Response body"))
        self.assertEqual(res.body.length, 13)
        self.assertEqual(res.body.read(32), b"Response body")
        self.assertEqual(res.body.length, 13)

        # Chunked
        res = HTTPResponse(True, "200", "Ok", "HTTP/1.1",
                           {"Argyle-Fishsticks": "Procrastinating",
                            "Transfer-Encoding": "chunked"},
                           content=io.BytesIO(b"f\r\nFifteen chars..\r\n4\r\n"
                                              b"four\r\n10\r\nSixteen chars..."
                                              b"\r\n0\r\n\r\n"))
        self.assertEqual(res.body.length, 35)
        self.assertEqual(res.body.read(64), b"Fifteen chars..fourSixteen "
                         b"chars...")
        self.assertEqual(res.body.length, 35)

    def test_get_status_line(self):
        """
        Tests the get_status_line() method of the HTTPResponse class.
        """
        res = HTTPResponse(True, "200", "Ok", "HTTP/1.1",
                           {"Argyle-Fishsticks": "Procrastinating",
                            "Content-Length": "0"},
                           content=io.BytesIO(b""))
        self.assertEqual(res.get_status_line(), b"HTTP/1.1 200 Ok")

        res = HTTPResponse(True, "404", "File Not Found", "HTTP/1.1",
                           {"Argyle-Fishsticks": "Procrastinating",
                            "Content-Length": "0"},
                           content=io.BytesIO(b""))
        self.assertEqual(res.get_status_line(), b"HTTP/1.1 404 File Not Found")
