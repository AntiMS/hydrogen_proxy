from http.client import parse_headers
from io import BytesIO
from unittest.mock import Mock

from .base import BaseTestCase

from hydrogen.signals import default_bus
from hydrogen.conf import get_layers
from hydrogen.contrib.admin.registry import default_admin, Admin, AdminModule
from hydrogen.contrib.admin import sender
from hydrogen.contrib import admin
from hydrogen.message import HTTPRequest


def patch_template_environment(f):
    """
    A decorator which patches the template_environment in the admin sender
    package.

    The unittest.mock.patch decorator cannot be used for this case because the
    hydrogen.contrib package has an attribute named "admin" *and* a subpackage
    named "admin" and the patch decorator always prefers an attribute over a
    subpackage. In this case, we need the template_environment variable in the
    *package* hydrogen.contrib.admin.sender patched. This custom patching
    function does what is needed for this use case.
    """
    def out(self, *args, **kwargs):
        orig_template_environment = sender.template_environment
        sender.template_environment = Mock()

        f(self, sender.template_environment, *args, **kwargs)

        sender.template_environment = orig_template_environment
    return out


class TestModule:
    """
    Represents a module to be registered to the admin.
    """
    def __init__(self, name):
        """
        Parameters
        ==========
        name
            Just an identifier for this fake module. Used when checking
            results.
        """
        self.name = name

    def __repr__(self):
        return 'TestModule("{}")'.format(self.name)

    def __eq__(self, other):
        if isinstance(other, TestModule):
            other = other.name
        elif not isinstance(other, str):
            return False
        return self.name == other


class TestAdmin(BaseTestCase):
    """
    Tests the admin module registry and other admin-interface-related
    infrastructure.
    """
    def setUp(self):
        """
        Sets up settings, options, clears the default admin, and imports tested
        classes.
        """
        super().setUp({"TEMPLATE_DIRS": (), "SETTINGS_DIR": "/tmp"})

        default_admin.reset()

    def test_admin_module(self):
        """
        Tests the AdminModule class.
        """
        a = Admin()
        m1 = TestModule("argyle")
        am1 = AdminModule(a, "argyle", m1, (), {}, [], [])
        am2 = AdminModule(a, "argyle", m1, (), {}, [], [])
        self.assertEqual(am1, am2)

        am2 = AdminModule(a, "bacon", m1, (), {}, [], [])
        self.assertNotEqual(am1, am2)

        m2 = TestModule("bacon")
        am1 = AdminModule(a, "argyle", m1, (), {}, [])
        am2 = AdminModule(a, "bacon", m2, (), {}, [am1])
        am1.parents.append(am2)
        self.assertTrue(am2.is_child(am1))
        self.assertTrue(am2.is_child(m1))

        am3 = AdminModule(a, "argyle", m1, (), {}, [])
        am4 = AdminModule(a, "bacon", m2, (), {}, [am3])
        am3.parents.append(am4)
        self.assertEqual(am2, am4)

        m3 = TestModule("canon")
        am5 = AdminModule(a, "argyle", m3, (), {}, [])
        am4 = AdminModule(a, "bacon", m2, (), {}, [am5])
        am5.parents.append(am4)
        self.assertNotEqual(am2, am4)

    def test_registry(self):
        """
        Tests the Admin class (the registry).
        """
        argyle = TestModule("argyle")
        bacon = TestModule("bacon")
        canon = TestModule("canon")
        devon = TestModule("devon")

        default_bus.trigger("module_created.bacon", "bacon", bacon, (), {}, ())
        default_bus.trigger("module_created.canon", "canon", canon, (), {}, ())
        default_bus.trigger("module_created.devon", "devon", devon, (), {}, ())

        instances = [m.instance for m in default_admin.modules]
        self.assertEqual(len(instances), 3)
        self.assertIn(bacon, instances)
        self.assertIn(canon, instances)
        self.assertIn(devon, instances)
        self.assertEqual([m.instance for m in default_admin.entry_points],
                         [bacon, canon, devon])
        names = [m.name for m in default_admin.modules]
        self.assertEqual(len(names), 3)
        self.assertIn("bacon", names)
        self.assertIn("canon", names)
        self.assertIn("devon", names)
        self.assertIs(default_admin.get_by_instance(bacon),
                      default_admin.get_by_name("bacon"))
        self.assertIs(default_admin.get_by_instance(canon),
                      default_admin.get_by_name("canon"))
        self.assertIs(default_admin.get_by_instance(devon),
                      default_admin.get_by_name("devon"))
        self.assertIs(default_admin.get_by_name("bacon").instance, bacon)
        self.assertIs(default_admin.get_by_name("canon").instance, canon)
        self.assertIs(default_admin.get_by_name("devon").instance, devon)
        self.assertEqual(default_admin.get_by_name("bacon").children, [])
        self.assertEqual(default_admin.get_by_name("canon").children, [])
        self.assertEqual(default_admin.get_by_name("devon").children, [])
        self.assertEqual(default_admin.get_by_name("bacon").parents, [])
        self.assertEqual(default_admin.get_by_name("canon").parents, [])
        self.assertEqual(default_admin.get_by_name("devon").parents, [])

        default_bus.trigger("module_created.argyle", "argyle", argyle, (), {},
                            (bacon, canon, devon))

        instances = [m.instance for m in default_admin.modules]
        self.assertEqual(len(instances), 4)
        self.assertIn(bacon, instances)
        self.assertIn(canon, instances)
        self.assertIn(devon, instances)
        self.assertIn(argyle, instances)
        self.assertEqual([m.instance for m in default_admin.entry_points],
                         [argyle])
        names = [m.name for m in default_admin.modules]
        self.assertEqual(len(names), 4)
        self.assertIn("bacon", names)
        self.assertIn("canon", names)
        self.assertIn("devon", names)
        self.assertIn("argyle", names)
        self.assertIs(default_admin.get_by_instance(bacon),
                      default_admin.get_by_name("bacon"))
        self.assertIs(default_admin.get_by_instance(canon),
                      default_admin.get_by_name("canon"))
        self.assertIs(default_admin.get_by_instance(devon),
                      default_admin.get_by_name("devon"))
        self.assertIs(default_admin.get_by_instance(argyle),
                      default_admin.get_by_name("argyle"))
        self.assertIs(default_admin.get_by_name("bacon").instance, bacon)
        self.assertIs(default_admin.get_by_name("canon").instance, canon)
        self.assertIs(default_admin.get_by_name("devon").instance, devon)
        self.assertIs(default_admin.get_by_name("argyle").instance, argyle)
        self.assertEqual(default_admin.get_by_name("bacon").children, [])
        self.assertEqual(default_admin.get_by_name("canon").children, [])
        self.assertEqual(default_admin.get_by_name("devon").children, [])
        self.assertEqual(default_admin.get_by_name("argyle").children,
                         [default_admin.get_by_name("bacon"),
                          default_admin.get_by_name("canon"),
                          default_admin.get_by_name("devon")])
        self.assertEqual(default_admin.get_by_name("bacon").parents,
                         [default_admin.get_by_name("argyle")])
        self.assertEqual(default_admin.get_by_name("canon").parents,
                         [default_admin.get_by_name("argyle")])
        self.assertEqual(default_admin.get_by_name("devon").parents,
                         [default_admin.get_by_name("argyle")])
        self.assertEqual(default_admin.get_by_name("argyle").parents, [])

        nameless = TestModule(None)
        default_bus.trigger("module_created.<anon>", None, nameless, (), {}, ()
                            )

        self.assertTrue(default_admin.entry_points[-1].name.startswith("mod_"))

    def _assert_sender_with_base_path(self, template_environment, base_path):
        """
        Performs dummy requests and responses against an admin sender and
        performs asserts necessary to test the admin sender. Also uses the
        given base_path when instantiating and testing the sender.

        Variables
        =========
        template_environment
            A reference to the mock object which has been patched into the
            hydrogen.contrib.admin.sender package.

        base_path
            The base path to use when instantiating and testing the admin
            sender.
        """
        argyle = TestModule("argyle")
        bacon = TestModule("bacon")
        default_bus.trigger("module_created.bacon", "bacon", bacon, (), {}, ())
        default_bus.trigger("module_created.argyle", "argyle", argyle, (), {},
                            (bacon,))
        argyle = AdminModule(default_admin, "argyle", argyle, (), {}, [])
        bacon = AdminModule(default_admin, "bacon", bacon, (), {}, [])
        argyle.children = [bacon]
        bacon.parents = [argyle]

        template_environment.get_template().render.return_value = "PAGE_OUTPUT"
        template_environment.reset_mock()

        admin_sender = admin(base_path=base_path)

        base_url = "http://hydrogen.proxy/"
        if len(base_path) > 0:
            base_url += base_path + "/"

        req = HTTPRequest(("127.0.0.1", 1234), "GET", base_url, "HTTP/1.1",
                          parse_headers(BytesIO(b"A: B\r\nC: D\r\n\r\n")),
                          BytesIO(b""))
        res = admin_sender(req).get_input(head=False).read(256)

        template_environment.get_template.assert_called_with(
                "contrib/admin/admin_home.html")
        template_environment.get_template().render.assert_called_with(
                base_path=base_path, modules=[argyle, bacon],
                receivers=[argyle])
        self.assertEqual(res, b"PAGE_OUTPUT")

        template_environment.reset_mock()

        req = HTTPRequest(("127.0.0.1", 1234), "GET",
                          base_url + "module/argyle", "HTTP/1.1",
                          parse_headers(BytesIO(b"A: B\r\nC: D\r\n\r\n")),
                          BytesIO(b""))
        res = admin_sender(req).get_input(head=False).read(256)

        template_environment.get_template.assert_called_with(
                "contrib/admin/admin_module_details.html")
        template_environment.get_template().render.assert_called_with(
                base_path=base_path, module=argyle)
        self.assertEqual(res, b"PAGE_OUTPUT")

        template_environment.reset_mock()

        req = HTTPRequest(("127.0.0.1", 1234), "GET",
                          base_url + "module/bacon", "HTTP/1.1",
                          parse_headers(BytesIO(b"A: B\r\nC: D\r\n\r\n")),
                          BytesIO(b""))
        res = admin_sender(req).get_input(head=False).read(256)

        template_environment.get_template.assert_called_with(
                "contrib/admin/admin_module_details.html")
        template_environment.get_template().render.assert_called_with(
                base_path=base_path, module=bacon)
        self.assertEqual(res, b"PAGE_OUTPUT")

        template_environment.reset_mock()

        req = HTTPRequest(("127.0.0.1", 1234), "GET", base_url + "settings",
                          "HTTP/1.1",
                          parse_headers(BytesIO(b"A: B\r\nC: D\r\n\r\n")),
                          BytesIO(b""))
        res = admin_sender(req).get_input(head=False).read(256)

        template_environment.get_template.assert_called_with(
                "contrib/admin/admin_settings.html")
        template_environment.get_template().render.assert_called_with(
                base_path=base_path, layers=get_layers())
        self.assertEqual(res, b"PAGE_OUTPUT")

    @patch_template_environment
    def test_sender(self, template_environment):
        """
        Tests the AdminSender's base_path functionality.
        """
        self._assert_sender_with_base_path(template_environment, "")
        default_admin.reset()
        self._assert_sender_with_base_path(template_environment, "base_path")
