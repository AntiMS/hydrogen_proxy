from http.client import parse_headers
from io import BytesIO
from unittest import TestCase

from hydrogen.handlers import (rmreqhdr, addreqhdr, setreqhdr, rmreshdr,
                               addreshdr, setreshdr)


class FakeTransaction:
    def __init__(self, headers):
        if isinstance(headers, bytes):
            headers = BytesIO(headers)
        if isinstance(headers, BytesIO):
            headers = parse_headers(headers)
        self.headers = headers


class FakeHandler:
    def _normalize_transaction(self, transaction):
        if transaction is not None and not isinstance(transaction,
                                                      FakeTransaction):
            return FakeTransaction(transaction)
        return transaction

    def __init__(self, response=None):
        self.response = self._normalize_transaction(response)

    def __call__(self, request):
        self.request = self._normalize_transaction(request)
        return self.response


class TestHeaderHandler(TestCase):
    """
    Tests the header modifying handlers.
    """
    def test_request_header_removing_handler(self):
        """
        Tests the RequestHeaderRemovingHandler.
        """
        child = FakeHandler()
        handler = rmreqhdr(child, "A", "C", "D", "F", "G")
        handler(FakeTransaction(b"A: A\nB: B\nC: C\nD: Z\nD: D\nE: Z\n"
                                b"E: E\n\n"))
        headers = child.request.headers

        self.assertEqual(len(headers), 3)
        self.assertEqual(headers.get_all("B"), ["B"])
        self.assertEqual(headers.get_all("E"), ["Z", "E"])
        self.assertIsNone(headers.get_all("A"))
        self.assertIsNone(headers.get_all("C"))
        self.assertIsNone(headers.get_all("D"))
        self.assertIsNone(headers.get_all("F"))
        self.assertIsNone(headers.get_all("G"))

        handler = rmreqhdr(child, "Content-Type", "Accept", "Referer")
        handler(FakeTransaction(b"Content-Type: text/html\nHost: "
                                b"example.com\nAccept: */*\nUser-Agent:"
                                b" Hydrogen/1.0\n\n"))
        headers = child.request.headers

        self.assertEqual(len(headers), 2)
        self.assertEqual(headers.get_all("Host"), ["example.com"])
        self.assertEqual(headers.get_all("User-Agent"), ["Hydrogen/1.0"])
        self.assertIsNone(headers.get_all("Content-Type"))
        self.assertIsNone(headers.get_all("Accept"))
        self.assertIsNone(headers.get_all("Referer"))

    def test_request_header_adding_handler(self):
        """
        Tests the RequestHeaderAddingHandler.
        """
        child = FakeHandler()
        handler = addreqhdr(child, {"A": "W", "C": "X", "D": "Y", "F": "F",
                                    "G": "G"})
        handler(FakeTransaction(b"A: A\nB: B\nC: C\nD: Z\nD: D\nE: Z\n"
                                b"E: E\n\n"))
        headers = child.request.headers

        self.assertEqual(len(headers), 12)
        self.assertEqual(headers.get_all("A"), ["A", "W"])
        self.assertEqual(headers.get_all("B"), ["B"])
        self.assertEqual(headers.get_all("C"), ["C", "X"])
        self.assertEqual(headers.get_all("D"), ["Z", "D", "Y"])
        self.assertEqual(headers.get_all("E"), ["Z", "E"])
        self.assertEqual(headers.get_all("F"), ["F"])
        self.assertEqual(headers.get_all("G"), ["G"])

        handler = addreqhdr(child, {"Content-Type": "application/json",
                                    "Accept": "text/html", "Referer":
                                    "http://example.com/referer"})
        handler(FakeTransaction(b"Content-Type: text/html\nHost: "
                                b"example.com\nAccept: */*\nUser-Agent:"
                                b" Hydrogen/1.0\n\n"))
        headers = child.request.headers

        self.assertEqual(len(headers), 7)
        self.assertEqual(headers.get_all("Content-Type"), ["text/html",
                                                           "application/json"])
        self.assertEqual(headers.get_all("Host"), ["example.com"])
        self.assertEqual(headers.get_all("Accept"), ["*/*", "text/html"])
        self.assertEqual(headers.get_all("User-Agent"), ["Hydrogen/1.0"])
        self.assertEqual(headers.get_all("Referer"),
                         ["http://example.com/referer"])

    def test_request_header_setting_handler(self):
        """
        Tests the RequestHeaderSettingHandler.
        """
        child = FakeHandler()
        handler = setreqhdr(child, {"A": "W", "C": "X", "D": "Y", "F": "F",
                                    "G": "G"})
        handler(FakeTransaction(b"A: A\nB: B\nC: C\nD: Z\nD: D\nE: Z\n"
                                b"E: E\n\n"))
        headers = child.request.headers

        self.assertEqual(len(headers), 8)
        self.assertEqual(headers.get_all("A"), ["W"])
        self.assertEqual(headers.get_all("B"), ["B"])
        self.assertEqual(headers.get_all("C"), ["X"])
        self.assertEqual(headers.get_all("D"), ["Y"])
        self.assertEqual(headers.get_all("E"), ["Z", "E"])
        self.assertEqual(headers.get_all("F"), ["F"])
        self.assertEqual(headers.get_all("G"), ["G"])

        handler = setreqhdr(child, {"Content-Type": "application/json",
                                    "Accept": "text/html", "Referer":
                                    "http://example.com/referer"})
        handler(FakeTransaction(b"Content-Type: text/html\nHost: "
                                b"example.com\nAccept: */*\nUser-Agent:"
                                b" Hydrogen/1.0\n\n"))
        headers = child.request.headers

        self.assertEqual(len(headers), 5)
        self.assertEqual(headers.get_all("Content-Type"), ["application/json"])
        self.assertEqual(headers.get_all("Host"), ["example.com"])
        self.assertEqual(headers.get_all("Accept"), ["text/html"])
        self.assertEqual(headers.get_all("User-Agent"), ["Hydrogen/1.0"])
        self.assertEqual(headers.get_all("Referer"),
                         ["http://example.com/referer"])

    def test_response_header_removing_handler(self):
        """
        Tests the ResponseHeaderRemovingHandler.
        """
        child = FakeHandler(b"A: A\nB: B\nC: C\nD: Z\nD: D\nE: Z\nE: E\n\n")
        handler = rmreshdr(child, "A", "C", "D", "F", "G")
        headers = handler(None).headers

        self.assertEqual(len(headers), 3)
        self.assertEqual(headers.get_all("B"), ["B"])
        self.assertEqual(headers.get_all("E"), ["Z", "E"])
        self.assertIsNone(headers.get_all("A"))
        self.assertIsNone(headers.get_all("C"))
        self.assertIsNone(headers.get_all("D"))
        self.assertIsNone(headers.get_all("F"))
        self.assertIsNone(headers.get_all("G"))

        child = FakeHandler(b"Content-Type: text/html\nHost: example.com\n"
                            b"Accept: */*\nUser-Agent: Hydrogen/1.0\n\n")
        handler = rmreshdr(child, "Content-Type", "Accept", "Referer")
        headers = handler(None).headers

        self.assertEqual(len(headers), 2)
        self.assertEqual(headers.get_all("Host"), ["example.com"])
        self.assertEqual(headers.get_all("User-Agent"), ["Hydrogen/1.0"])
        self.assertIsNone(headers.get_all("Content-Type"))
        self.assertIsNone(headers.get_all("Accept"))
        self.assertIsNone(headers.get_all("Referer"))

    def test_response_header_adding_handler(self):
        """
        Tests the ResponseHeaderAddingHandler.
        """
        child = FakeHandler(b"A: A\nB: B\nC: C\nD: D\nE: E\n\n")
        handler = addreshdr(child, {"A": "W", "C": "X", "D": "Y", "F": "F",
                                    "G": "G"})
        headers = handler(None).headers

        self.assertEqual(len(headers), 10)
        self.assertEqual(headers.get_all("A"), ["A", "W"])
        self.assertEqual(headers.get_all("B"), ["B"])
        self.assertEqual(headers.get_all("C"), ["C", "X"])
        self.assertEqual(headers.get_all("D"), ["D", "Y"])
        self.assertEqual(headers.get_all("E"), ["E"])
        self.assertEqual(headers.get_all("F"), ["F"])
        self.assertEqual(headers.get_all("G"), ["G"])

        child = FakeHandler(b"Content-Type: text/html\nTransfer-Encoding: "
                            b"chunked\nSet-Cookie: cookiename=cookievalue\n"
                            b"Server: Hydrogen/1.0\n\n")
        handler = addreshdr(child, {"Content-Type": "application/json",
                                    "Set-Cookie": "newcookie=newvalue",
                                    "Content-Encoding": "gzip"})
        headers = handler(None).headers

        self.assertEqual(len(headers), 7)
        self.assertEqual(headers.get_all("Content-Type"), ["text/html",
                                                           "application/json"])
        self.assertEqual(headers.get_all("Transfer-Encoding"), ["chunked"])
        self.assertEqual(headers.get_all("Set-Cookie"),
                         ["cookiename=cookievalue", "newcookie=newvalue"])
        self.assertEqual(headers.get_all("Server"), ["Hydrogen/1.0"])
        self.assertEqual(headers.get_all("Content-Encoding"), ["gzip"])

    def test_response_header_setting_handler(self):
        """
        Tests the ResponseHeaderSettingHandler.
        """
        child = FakeHandler(b"A: A\nB: B\nC: C\nD: D\nE: E\n\n")
        handler = setreshdr(child, {"A": "W", "C": "X", "D": "Y", "F": "F",
                                    "G": "G"})
        headers = handler(None).headers

        self.assertEqual(len(headers), 7)
        self.assertEqual(headers.get_all("A"), ["W"])
        self.assertEqual(headers.get_all("B"), ["B"])
        self.assertEqual(headers.get_all("C"), ["X"])
        self.assertEqual(headers.get_all("D"), ["Y"])
        self.assertEqual(headers.get_all("E"), ["E"])
        self.assertEqual(headers.get_all("F"), ["F"])
        self.assertEqual(headers.get_all("G"), ["G"])

        child = FakeHandler(b"Content-Type: text/html\nTransfer-Encoding: "
                            b"chunked\nSet-Cookie: cookiename=cookievalue\n"
                            b"Server: Hydrogen/1.0\n\n")
        handler = setreshdr(child, {"Content-Type": "application/json",
                                    "Set-Cookie": "newcookie=newvalue",
                                    "Content-Encoding": "gzip"})
        headers = handler(None).headers

        self.assertEqual(len(headers), 5)
        self.assertEqual(headers.get_all("Content-Type"), ["application/json"])
        self.assertEqual(headers.get_all("Transfer-Encoding"), ["chunked"])
        self.assertEqual(headers.get_all("Set-Cookie"), ["newcookie=newvalue"])
        self.assertEqual(headers.get_all("Server"), ["Hydrogen/1.0"])
        self.assertEqual(headers.get_all("Content-Encoding"), ["gzip"])
