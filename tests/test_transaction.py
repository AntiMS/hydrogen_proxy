from http.client import HTTPMessage
import io
from unittest import TestCase

from hydrogen.message.transaction import HTTPTransaction


class TestTransaction(TestCase):
    """
    Tests the HTTPTransaction class.
    """
    def test_length(self):
        """
        Tests that the HTTPTransaction class sets its length property
        correctly.
        """
        class MockBody:
            def __init__(self, l):
                self.length = l

        # Setting the body via the content.
        class MockContentTransaction(HTTPTransaction):
            def __init__(self, l, *args, **kwargs):
                self._length = l
                super().__init__(*args, **kwargs)

            def get_body(self, *args, **kwargs):
                return MockBody(self._length)
        transaction = MockContentTransaction(999, "HTTP/1.1",
                                             {"Content-Length": "256"})
        self.assertEqual(transaction.length, 999)

        # Setting the body directly.
        transaction = HTTPTransaction("HTTP/1.1", {"Content-Length": "256"},
                                      body=MockBody(555))
        self.assertEqual(transaction.length, 555)

    def _get_mock_transaction(self, status_line, *args, **kwargs):
        """
        A helper which instantiates and returns a simple HTTPTransaction
        subclass.
        """
        class MockTransaction(HTTPTransaction):
            def __init__(self, *args, **kwargs):
                self.status_line_args = None
                super().__init__(*args, **kwargs)

            def get_status_line(self, *args, **kwargs):
                self.status_line_args = (args, kwargs)
                return status_line

        args = list(args)
        headers = HTTPMessage()
        headerarg = args[1].items() if hasattr(args[1], "items") else args[1]
        for k, v in headerarg:
            headers[k] = v
        args[1] = headers

        return MockTransaction(*args, **kwargs)

    def test_head(self):
        """
        Tests the head method.
        """
        transaction = self._get_mock_transaction(b"status_line", "HTTP/1.1",
                                                 {"Content-Length": "256"},
                                                 body=True)
        self.assertEqual(transaction.get_head(1, 2, 3, one=1, two=2, three=3),
                         b"status_line\r\nContent-Length: 256\r\n\r\n")
        self.assertEqual(transaction.status_line_args, ((1, 2, 3),
                         {"one": 1, "two": 2, "three": 3}))

        transaction = self._get_mock_transaction(b"status_line", "HTTP/1.1",
                                                 (("A", "A"), ("A", "B")),
                                                 body=True)
        self.assertEqual(transaction.get_head(), b"status_line\r\nA: A\r\nA: B"
                         b"\r\n\r\n")
        self.assertEqual(transaction.status_line_args, ((), {}))

    def test_input(self):
        """
        Tests the input method.
        """
        body_bytes = b"Transaction Body"

        # head == False
        body = io.BytesIO(body_bytes)
        transaction = self._get_mock_transaction(b"status_line", "HTTP/1.1",
                                                 {"Argyle-Fishsticks":
                                                  "Procrastinating"},
                                                 body=body)
        input = transaction.get_input(head=False)
        self.assertEqual(input.read(len(body_bytes) * 2), body_bytes)

        # head == True
        body = io.BytesIO(body_bytes)
        transaction = self._get_mock_transaction(b"status_line", "HTTP/1.1",
                                                 {"Argyle-Fishsticks":
                                                  "Procrastinating"},
                                                 body=body)
        input = transaction.get_input(head=True)
        result = b"status_line\r\nArgyle-Fishsticks: " + \
                 b"Procrastinating\r\n\r\n" + body_bytes
        self.assertEqual(input.read(len(result) * 2), result)

    def test_clone(self):
        """
        Tests the clone method.
        """
        class MockTransaction(HTTPTransaction):
            ATTRS = ("skunk",)

            def __init__(self, skunk, *args, **kwargs):
                self.skunk = skunk
                super().__init__(*args, **kwargs)

            def get_body(self, *args, **kwargs):
                return b"body"
        transaction = MockTransaction(b"Pepe", "HTTP/1.1",
                                      {"Argyle-Fishsticks": "Procrastinating"},
                                      content=True, tags={"platypus"})
        clone = transaction.clone(content=False)
        self.assertEqual(clone.skunk, transaction.skunk)
        self.assertEqual(clone.http_version, transaction.http_version)
        self.assertEqual(clone.headers, transaction.headers)
        self.assertEqual(clone.tags, transaction.tags)
        self.assertFalse(clone.content)
