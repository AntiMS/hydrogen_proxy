from unittest import TestCase

from hydrogen.helpers import (module, InstantiableModule, ModuleReference,
                              BaseModule)


class UTModule:
    """
    A basic module used for testing.
    """
    def __init__(self, one, two):
        self.one = one
        self.two = two

    def __eq__(self, other):
        return self.one == other.one and self.two == other.two


class TestHelpers(TestCase):
    """
    Tests the configuration helpers in hydrogen.helpers .
    """
    def test_module_function(self):
        """
        Tests the function "module".
        """
        def get_process_args():
            """
            Returns a fake implementation of process_args along with a list to
            which the fake implementation appends the argument with which it is
            called each time it is called.
            """
            vals = []

            def process_args(val):
                vals.append(val)
                return val

            return process_args, vals

        # ModuleReference instances
        process_args, vals = get_process_args()
        ref_module = module("name")
        self.assertIsInstance(ref_module, ModuleReference)
        self.assertEqual(ref_module.name, "name")
        self.assertEqual(ref_module.get_module(process_args), "name")
        self.assertEqual(len(vals), 0)

        process_args, vals = get_process_args()
        ref_module = module(name="name")
        self.assertIsInstance(ref_module, ModuleReference)
        self.assertEqual(ref_module.name, "name")
        self.assertEqual(ref_module.get_module(process_args), "name")
        self.assertEqual(len(vals), 0)

        # InstantiableModule instances
        process_args, vals = get_process_args()
        inst_module = module(UTModule, ("one",), {"two": "two"})
        self.assertIsInstance(inst_module, InstantiableModule)
        self.assertEqual(inst_module.class_, UTModule)
        self.assertEqual(inst_module.args, ("one",))
        self.assertEqual(inst_module.kwargs, {"two": "two"})
        self.assertEqual(inst_module.get_module(process_args), UTModule("one",
                                                                        "two"))
        self.assertEqual(vals, [("one",), {"two": "two"}])

        process_args, vals = get_process_args()
        inst_module = module(class_=UTModule, args=("one",), kwargs={"two":
                                                                     "two"})
        self.assertIsInstance(inst_module, InstantiableModule)
        self.assertEqual(inst_module.class_, UTModule)
        self.assertEqual(inst_module.args, ("one",))
        self.assertEqual(inst_module.kwargs, {"two": "two"})
        self.assertEqual(inst_module.get_module(process_args), UTModule("one",
                                                                        "two"))
        self.assertEqual(vals, [("one",), {"two": "two"}])

        process_args, vals = get_process_args()
        inst_module = module(class_=UTModule, kwargs={"one": "one",
                                                      "two": "two"})
        self.assertIsInstance(inst_module, InstantiableModule)
        self.assertEqual(inst_module.class_, UTModule)
        self.assertEqual(inst_module.args, ())
        self.assertEqual(inst_module.kwargs, {"one": "one", "two": "two"})
        self.assertEqual(inst_module.get_module(process_args), UTModule("one",
                                                                        "two"))
        self.assertEqual(vals, [(), {"one": "one", "two": "two"}])

        process_args, vals = get_process_args()
        inst_module = module(class_=UTModule, args=("one", "two"))
        self.assertIsInstance(inst_module, InstantiableModule)
        self.assertEqual(inst_module.class_, UTModule)
        self.assertEqual(inst_module.args, ("one", "two"))
        self.assertEqual(inst_module.kwargs, {})
        self.assertEqual(inst_module.get_module(process_args), UTModule("one",
                                                                        "two"))
        self.assertEqual(vals, [("one", "two"), {}])

        process_args, vals = get_process_args()
        inst_module = module(class_=UTModule, args=("one", "two"))
        self.assertIsInstance(inst_module, InstantiableModule)
        self.assertEqual(inst_module.class_, UTModule)
        self.assertEqual(inst_module.args, ("one", "two"))
        self.assertEqual(inst_module.kwargs, {})
        self.assertEqual(inst_module.get_module(process_args), UTModule("one",
                                                                        "two"))
        self.assertEqual(vals, [("one", "two"), {}])

        # Invalid calls to the module function
        self.assertRaises(AssertionError, module)
        self.assertRaises(AssertionError, module, "a", "b")
        self.assertRaises(AssertionError, module, "a", "b", "c", "d")
        self.assertRaises(AssertionError, module, a="a")
        self.assertRaises(AssertionError, module, name="name", args="args")
        self.assertRaises(AssertionError, module, "class", args="args")

    def test_operators(self):
        """
        Tests that the Python magic functions for overriding operators for
        modules function as expected.
        """
        class TestModule(BaseModule):
            def __init__(self, val):
                self.val = val

            def get_module(self, process_args):
                return self.val

        def process_args(val):
            if isinstance(val, TestModule):
                val = val.get_module(process_args)
            return val

        self.assertEqual((TestModule(10) + 8).get_module(process_args), 18)
        self.assertEqual((TestModule(6) & 10).get_module(process_args), 2)
        self.assertEqual((TestModule(5) * 3).get_module(process_args), 15)
        self.assertEqual((TestModule(6) | 10).get_module(process_args), 14)
        self.assertEqual((TestModule(3) ** 3).get_module(process_args), 27)
        self.assertEqual((10 + TestModule(8)).get_module(process_args), 18)
        self.assertEqual((6 & TestModule(10)).get_module(process_args), 2)
        self.assertEqual((5 * TestModule(3)).get_module(process_args), 15)
        self.assertEqual((6 | TestModule(10)).get_module(process_args), 14)
        self.assertEqual((3 ** TestModule(3)).get_module(process_args), 27)
        self.assertEqual((8 - TestModule(3)).get_module(process_args), 5)
        self.assertEqual((6 ^ TestModule(10)).get_module(process_args), 12)
        self.assertEqual((TestModule(8) - 3).get_module(process_args), 5)
        self.assertEqual((TestModule(6) ^ 10).get_module(process_args), 12)
        self.assertEqual((~TestModule(5)).get_module(process_args), -6)
        self.assertEqual((-TestModule(18)).get_module(process_args), -18)
        self.assertEqual((+TestModule(18)).get_module(process_args), 18)
