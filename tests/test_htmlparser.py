from unittest import TestCase

from hydrogen.handlers.html.parser import HydrogenHTMLParser
from hydrogen.handlers.html.tree import (RootNode, TagNode, SelfClosingTagNode,
                                         CharrefNode, EntityrefNode, DataNode,
                                         CommentNode, DeclNode, PiNode)


_TEST_HTML_DOCUMENT = """
<!DOCTYPE html><?test>
<html>
    <head><title>Title</title></head>
    <body class="argyle">
        <h1>Testing</h1><p>This is a test</p><p></p>
        <hr><ul><li>One<li>Two<li>Three</li><li>Four</ul>
        &amp;&#97;<!--This is a comment-->
        &lt;object&gt;
    </body>
</html>
"""

_EXPECTED_HTML_DOCUMENT = """
<!DOCTYPE html><?test>
<html>
    <head><title>Title</title></head>
    <body class="argyle">
        <h1>Testing</h1><p>This is a test</p><p></p>
        <hr/><ul><li>One</li><li>Two</li><li>Three</li><li>Four</li></ul>
        &amp;&#97;<!--This is a comment-->
        &lt;object&gt;
    </body>
</html>
"""


class TestHTMLParser(TestCase):
    """
    Tests the HydrogenHTMLParser object.
    """
    def test_build_tree(self):
        """
        Tests the tree building functionality.
        """
        tree = RootNode()
        parser = HydrogenHTMLParser(tree, convert_charrefs=False)

        parser.feed(_TEST_HTML_DOCUMENT)

        def check_tree(tree, results):
            self.assertEqual(len(tree.children), len(results))
            for c, r in zip(tree.children, results):
                self.assertIsInstance(c, r["type"])
                self.assertTrue(r["check"](c))
                if "children" in r:
                    check_tree(c, r["children"])
                else:
                    self.assertFalse(hasattr(c, "children"))

        def data(text):
            return {"type": DataNode, "check": lambda n: n.data == text}

        def tag(tagname, attrs={}, children=None):
            def check(n):
                if n.tagname != tagname:
                    return False
                if len(n.attrs) != len(attrs):
                    return False
                for k, v in n.attrs.items():
                    if v != attrs[k]:
                        return False
                return True
            out = {"type": TagNode, "check": check}
            if children is not None:
                out["children"] = children
            return out

        check_tree(tree, [
            data("\n"),
            {"type": DeclNode, "check": lambda n: n.decl == "DOCTYPE html"},
            {"type": PiNode, "check": lambda n: n.data == "test"},
            data("\n"),
            tag("html", children=[
                data("\n    "),
                tag("head", children=[tag("title", children=[data("Title")])]),
                data("\n    "),
                tag("body", attrs={"class": "argyle"}, children=[
                    data("\n        "),
                    tag("h1", children=[data("Testing")]),
                    tag("p", children=[data("This is a test")]),
                    tag("p", children=[]),
                    data("\n        "),
                    {"type": SelfClosingTagNode, "check": lambda n:
                        n.tagname == "hr" and len(n.attrs) == 0},
                    tag("ul", children=[
                        tag("li", children=[data("One")]),
                        tag("li", children=[data("Two")]),
                        tag("li", children=[data("Three")]),
                        tag("li", children=[data("Four")]),
                        ]),
                    data("\n        "),
                    {"type": EntityrefNode, "check": lambda n:
                        n.name == "amp"},
                    {"type": CharrefNode, "check": lambda n: n.name == "97"},
                    {"type": CommentNode, "check":
                        lambda n: n.comment == "This is a comment"},
                    data("\n        "),
                    {"type": EntityrefNode, "check": lambda n: n.name == "lt"},
                    data("object"),
                    {"type": EntityrefNode, "check": lambda n: n.name == "gt"},
                    data("\n    ")
                ]),
                data("\n")
            ]),
            data("\n")
        ])

        self.assertEqual(tree.to_string(), _EXPECTED_HTML_DOCUMENT)
