from http.client import HTTPMessage
from io import BytesIO
import os
import tempfile
from unittest import TestCase

from hydrogen.message import HTTPRequest
from hydrogen.senders import file


class TestFileSender(TestCase):
    """
    Tests the FileSender.
    """
    def test_all(self):
        """
        Tests the sender.
        """
        def do_test(contents, output, *args):
            tf = tempfile.mkstemp()[1]
            with open(tf, "wb") as f:
                f.write(contents)

            request = HTTPRequest("127.0.0.1", "GET", "http://example.com/",
                                  "HTTP/1.1", HTTPMessage(), BytesIO())
            response = file(tf, *args)(request)

            self.assertEqual(response.get_input(header=True).read(65536),
                             output)

            os.remove(tf)

        do_test(b"\x00\x01\x02\xfd\xfe\xff", b"HTTP/1.1 200 Ok\r\nServer: "
                b"Hydrogen Proxy\r\nContent-Type: application/octet-stream\r\n"
                b"Content-Length: 6\r\n\r\n\x00\x01\x02\xfd\xfe\xff")
        do_test(b"This is a test.", b"HTTP/1.1 200 Ok\r\nServer: Hydrogen "
                b"Proxy\r\nContent-Type: text/plain\r\nContent-Length: 15\r\n"
                b"\r\nThis is a test.", "text/plain")
        do_test(b"No content type.", b"HTTP/1.1 200 Ok\r\nServer: Hydrogen "
                b"Proxy\r\nContent-Length: 16\r\n\r\nNo content type.", None)
