from unittest import TestCase

from hydrogen.handlers.html.tree import (HTMLNode, RootNode, ParentNode,
                                         TagNode, TagMixin, SelfClosingTagNode,
                                         CharrefNode, EntityrefNode, DataNode,
                                         CommentNode, DeclNode, PiNode)


class TestTree(TestCase):
    """
    Tests the classes that make up the tree object.
    """
    def test_html_node(self):
        """
        Tests the HTMLNode class.
        """
        node1 = HTMLNode()
        node2 = HTMLNode()
        parent = RootNode()

        self.assertIsNone(node1.parent)
        self.assertIsNone(node2.parent)
        self.assertIsNone(node1.root)
        self.assertIsNone(node2.root)

        parent.add_child(node1)
        self.assertEqual(node1.position, 0)
        self.assertEqual(node1.parent, parent)
        self.assertEqual(node1.root, parent)
        self.assertIsNone(node2.parent)
        self.assertIsNone(node2.root)

        parent.add_child(node2)
        self.assertEqual(node1.position, 0)
        self.assertEqual(node1.parent, parent)
        self.assertEqual(node1.root, parent)
        self.assertEqual(node2.position, 1)
        self.assertEqual(node2.parent, parent)
        self.assertEqual(node2.root, parent)

        self.assertEqual(node1.next_sibling(), node2)
        self.assertEqual(node2.prev_sibling(), node1)
        self.assertIsNone(node1.prev_sibling())
        self.assertIsNone(node2.next_sibling())

        node1.remove()
        self.assertEqual(parent.node_count, 1)
        self.assertIsNone(node1.parent)
        self.assertIsNone(node1.root)
        self.assertEqual(node2.position, 0)
        self.assertIsNone(node2.prev_sibling())
        self.assertIsNone(node2.next_sibling())

    def test_parent_node(self):
        """
        Tests the ParentNode class.
        """
        class TestHTMLNode(HTMLNode):
            def to_string(self):
                return "Node" + str(self.position)

        root = RootNode()
        parent = ParentNode()
        node0 = TestHTMLNode()
        node1 = TestHTMLNode()
        node2 = TestHTMLNode()

        root.add_child(parent)
        parent.add_child(node1)
        parent.add_child(node2)
        parent.add_child(node0, 0)

        self.assertEqual(node0.root, root)
        self.assertEqual(node1.root, root)
        self.assertEqual(node2.root, root)

        self.assertEqual(node0.position, 0)
        self.assertEqual(node1.position, 1)
        self.assertEqual(node2.position, 2)

        self.assertEqual(node0.parent, parent)
        self.assertEqual(node1.parent, parent)
        self.assertEqual(node2.parent, parent)

        self.assertEqual(parent.children[0], node0)
        self.assertEqual(parent.children[1], node1)
        self.assertEqual(parent.children[2], node2)

        self.assertEqual(parent.to_string(), "Node0Node1Node2")

        self.assertEqual(root.node_count, 4)

    def test_root_node(self):
        """
        Tests the RootNode class.
        """
        class TestHTMLNode(HTMLNode):
            def to_string(self):
                return "Node" + str(self.position)

        root = RootNode()
        node1 = TestHTMLNode()
        node2 = TagNode("tagname", {})
        node3 = TagNode("tagname", {})

        root.add_child(node1)

        self.assertEqual(root.node_count, 1)
        self.assertEqual(root.closed_node_count, 1)
        self.assertTrue(root.complete)

        root.add_child(node2)

        self.assertEqual(root.node_count, 2)
        self.assertEqual(root.closed_node_count, 1)
        self.assertFalse(root.complete)

        node2.close()

        self.assertEqual(root.node_count, 2)
        self.assertEqual(root.closed_node_count, 2)
        self.assertTrue(root.complete)

        root.add_child(node3)

        self.assertEqual(root.node_count, 3)
        self.assertEqual(root.closed_node_count, 2)
        self.assertFalse(root.complete)

        root.close_all()

        self.assertEqual(root.node_count, 3)
        self.assertEqual(root.closed_node_count, 3)
        self.assertTrue(root.complete)

        self.assertEqual(root.start_to_string(), "")
        self.assertEqual(root.children_to_string(),
                         "Node0<tagname></tagname><tagname></tagname>")
        self.assertEqual(root.end_to_string(), "")

    def test_tag_mixin(self):
        """
        Tests the TagMixin class.
        """
        tag = TagMixin("tagname", {"a": "b", "c": "d"})
        valid_strings = ("<tagname a=\"b\" c=\"d\"",
                         "<tagname c=\"d\" a=\"b\"")
        self.assertIn(tag.tagstart_to_string(), valid_strings)

    def test_tag_node(self):
        """
        Tests the TagNode class.
        """
        root = RootNode()
        node = TagNode("tagname", {"a": "b", "c": "d"})

        self.assertFalse(node.closed)
        self.assertFalse(node.serialized)

        root.add_child(node)

        self.assertEqual(root.node_count, 1)
        self.assertEqual(root.closed_node_count, 0)
        self.assertFalse(node.closed)
        self.assertFalse(node.serialized)

        valid_strings = ("<tagname a=\"b\" c=\"d\">",
                         "<tagname c=\"d\" a=\"b\">")
        self.assertIn(node.start_to_string(), valid_strings)
        self.assertEqual(node.end_to_string(), "</tagname>")
        self.assertEqual(node.children_to_string(), "")
        self.assertIn(node.to_string(), [v + "</tagname>" for v in
                                         valid_strings])

        node.add_child(EntityrefNode("amp"))

        self.assertEqual(root.node_count, 2)
        self.assertEqual(root.closed_node_count, 1)
        self.assertFalse(node.closed)
        self.assertFalse(node.serialized)

        self.assertIn(node.start_to_string(), valid_strings)
        self.assertEqual(node.end_to_string(), "</tagname>")
        self.assertEqual(node.children_to_string(), "&amp;")
        self.assertIn(node.to_string(), [v + "&amp;</tagname>" for v in
                                         valid_strings])

        node.close()

        self.assertEqual(root.node_count, 2)
        self.assertEqual(root.closed_node_count, 2)
        self.assertTrue(node.closed)
        self.assertFalse(node.serialized)

        self.assertIn(node.start_to_string(), valid_strings)
        self.assertEqual(node.end_to_string(), "</tagname>")
        self.assertEqual(node.children_to_string(), "&amp;")
        self.assertIn(node.to_string(), [v + "&amp;</tagname>" for v in
                                         valid_strings])

        node.serialized = True

        self.assertTrue(node.serialized)

    def test_self_closing_tag_node(self):
        """
        Tests the SelfClosingTagNode class.
        """
        tag = SelfClosingTagNode("tagname", {"a": "b", "c": "d"})
        valid_strings = ("<tagname a=\"b\" c=\"d\"/>",
                         "<tagname c=\"d\" a=\"b\"/>")
        self.assertIn(tag.to_string(), valid_strings)

    def test_charref_node(self):
        """
        Tests the CharrefNode class.
        """
        tag = CharrefNode("97")
        self.assertEqual(tag.to_string(), "&#97;")

    def test_entityref_node(self):
        """
        Tests the EntityrefNode class.
        """
        tag = EntityrefNode("amp")
        self.assertEqual(tag.to_string(), "&amp;")

    def test_data_node(self):
        """
        Tests the DataNode class.
        """
        tag = DataNode("This is a test.")
        self.assertEqual(tag.to_string(), "This is a test.")

    def test_comment_node(self):
        """
        Tests the CommentNode class.
        """
        tag = CommentNode("This is a comment.")
        self.assertEqual(tag.to_string(), "<!--This is a comment.-->")

    def test_decl_node(self):
        """
        Tests the DeclNode class.
        """
        tag = DeclNode("DOCTYPE html")
        self.assertEqual(tag.to_string(), "<!DOCTYPE html>")

    def test_pi_node(self):
        """
        Tests the PiNode class.
        """
        tag = PiNode("xml version=\"1.0\" encoding=\"UTF-8\"")
        self.assertEqual(tag.to_string(),
                         "<?xml version=\"1.0\" encoding=\"UTF-8\">")
