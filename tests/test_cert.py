import os
import shutil
import ssl
import tempfile

from .base import BaseTestCase


_CAKEY = b"""-----BEGIN PRIVATE KEY-----
MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANUm9im34t3kRghC
baifx3HUG1Q5WRsvZ8ae0km0PDTNy0tweI05/EyIE4Z4XX4WMfnqQqaY+6fBwxX4
4CpGXZ6bJ5oTLjbc3QGo2HPZ/9h25kVEJHKne58iDS2+lf/h9111fDcFNvuH1pul
9ebjt/GvKaefIKemnJpp1FSjzd6xAgMBAAECgYEApBEqV3BIev1zfnYLTEk4ToCy
TIEb+lXh7jtfqAXIdeiV+8plsGcasZnoVZAefqf87d4IUY3d3wHA9Q+tFD03Xy92
Qitquox6ZI2j7M5pxDWcvEeUaaR4hqKRncLfy/wUtZ7BSmOUqOT4tfhBCbLmmjj5
mXu6Oht5khOrdGHy8OUCQQDq63wdxRK8l9NzswZ/n3qDokAR+fRLDVHX7vzp7qG+
Oru4yB5SlLZ3HjADY0aVptrtI3xr/xQt8wCoDudnHcjjAkEA6EdvjvxgoI6oqxKg
sYN00McEfSxw8uiziB74eN9hH8ziByBbcAV3FGWF+T7tXI3Ky3wHiYbPR572AYs/
oKKSWwJAG+JVuibhQXd4BtyyIcXpQeSpXDjiEWKEW3AnnxDPLAhGrfk6+CxkuCDi
LBTFlQ/YHpu+sgb1+zpD0q7qMqgrZwJBAITpo5dhKPWWzEMf5J3PCAATcakizfrh
VyHz/uBDijMFLO1BAbGHHw2nzlLCRFZquHlZnoJ+houIFWLqyFzlyfMCQA454kkO
3HaM96ZJ4HTHujLhzydpa9k/t+Zuiv9gH+n3nKMjzL8vYMlMVq9/HB/APzWQsMkk
XVtFmB1H6D+AJWs=
-----END PRIVATE KEY-----
"""
_CACERT = b"""-----BEGIN CERTIFICATE-----
MIICZjCCAc+gAwIBAgIJANqumSBrt2Z8MA0GCSqGSIb3DQEBBQUAMEwxCzAJBgNV
BAYTAlVTMSYwJAYDVQQKDB1TY3JpcHQgUHJveHkgUm9vdCBDZXJ0aWZpY2F0ZTEV
MBMGA1UEAwwMU2NyaXB0IFByb3h5MB4XDTE0MDQyMDE2MjEwMloXDTE3MDIwNzE2
MjEwMlowTDELMAkGA1UEBhMCVVMxJjAkBgNVBAoMHVNjcmlwdCBQcm94eSBSb290
IENlcnRpZmljYXRlMRUwEwYDVQQDDAxTY3JpcHQgUHJveHkwgZ8wDQYJKoZIhvcN
AQEBBQADgY0AMIGJAoGBANUm9im34t3kRghCbaifx3HUG1Q5WRsvZ8ae0km0PDTN
y0tweI05/EyIE4Z4XX4WMfnqQqaY+6fBwxX44CpGXZ6bJ5oTLjbc3QGo2HPZ/9h2
5kVEJHKne58iDS2+lf/h9111fDcFNvuH1pul9ebjt/GvKaefIKemnJpp1FSjzd6x
AgMBAAGjUDBOMB0GA1UdDgQWBBR3/TaIOBb0jk4NISHRGtfihIRobTAfBgNVHSME
GDAWgBR3/TaIOBb0jk4NISHRGtfihIRobTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3
DQEBBQUAA4GBAC5sNZ7+i6lz7mwVtNm+16wvmwvwXF1fXgcIZg/7E69DdLpvpyKS
bybNPc+pp5kNcGyGEUh4sdsustHWnfYfZwWBnfj8YztbLd3Ut8jaPc+hLfyWh+L/
D2nDKmkT7cYaf/J+XkxEZoY7f2LnqGDKJwqO1nzl8Hd6xc5K+ASbnvQF
-----END CERTIFICATE-----
"""
_CKEY = b"""-----BEGIN PRIVATE KEY-----
MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAMViEEffGoGWGT5Q
1JqX/dFOQ7v08VDXxIBiKHq/ikCIihJ75/tBgPHnmFn5SBEUYA9HUSr44Hx/G/Zz
3XTlvTt6zkWnyAs/GcjxU9CQ+Kegdrmzkn5D+h9l3MjzUHZU5k/b6eldzdUNqtHU
aTHP65D525u7FfqFQObmqGnUl5mzAgMBAAECgYEAoqd67x8lAqCkULvfVz3lBbRP
EPZ6XbpQzdMVtIFe9msODMlak6yZC79jxtEr2BdISY7ljlnk8/LzIha86fXgB09h
iVtyc+JJkV/Cwy3R/hu5X+Vnd6kr5JJ6SAkRxOQfYmp1pTKY3bY2DikP7XTfejOO
g9NvxrBNnO/WQN9a2FECQQDh0yFAFGYOpujGeZwJWJvdAVac5Q2u9l6p1h69Me6T
SOjjxn20XdPcTIvmi02CryyNbsEE6biSoULO3yC1AeeVAkEA38IE5Z8m2XzD7ao/
rQ7OXp4gUt8rRvIJAISg/9+IdVfvw1xLlqM0/SHX2LwGdyKNkrtzNFv41ZVn3cny
JtuKJwJACTpNY4bx3KtpW6Lgg2lBhThmx6Z8dqfOmnYLb4O40f/qtFQKEvK6k4f1
N4h9tFdgJK9f58l9J1C7oCRx+7OGOQJBAJ+vxHTnPv+Jxf4wOR9VaLwn12OtNyJ7
iycegBP7wFQWPNtMW/mWYjOg2n0nuul89FJaip48YPp0lNq4y6MlnJMCQQCHQ+1w
p+dgjNoL+SUDXgYwCwEzbhAcM4YGP+BV4nebPu8XrziPEzdRhmIUCFnqj6hekt3b
90GlZxfx9aY0SLM9
-----END PRIVATE KEY-----
"""


class TestCert(BaseTestCase):
    """
    Tests certificate-related infrastructure.
    """
    def setUp(self):
        """
        Imports the function get_cert().
        """
        self.dir = tempfile.mkdtemp()
        self.certsdir = os.path.join(self.dir, "certs")
        cafile = os.path.join(self.dir, "ca.pem")
        ckeyfile = os.path.join(self.dir, "ckey.pem")
        os.mkdir(self.certsdir)
        with open(cafile, "wb") as f:
            f.write(_CAKEY)
            f.write(_CACERT)
        with open(ckeyfile, "wb") as f:
            f.write(_CKEY)

        super().setUp({"ROOT_CERT_FILE": cafile, "KEY_FILE": ckeyfile,
                       "CERTS_DIR": self.certsdir, "SETTINGS_DIR": self.dir})

        global get_cert
        from hydrogen.cert import reset, get_cert
        reset()

    def tearDown(self):
        """
        Cleans up the temporary directory after the test is done.
        """
        shutil.rmtree(self.dir)

    def test_get_cert(self):
        """
        Tests the get_cert function.
        """
        certfile = get_cert("www.example.com")
        self.assertEqual(certfile, os.path.join(self.certsdir,
                                                "www.example.com.pem"))
        with open(certfile, "rb") as f:
            contents = f.read()
        file = tempfile.mkstemp()[1]
        with open(file, "wb") as f:
            f.write(contents)
            f.write(_CACERT)
        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        context.load_cert_chain(file)
        os.remove(file)
