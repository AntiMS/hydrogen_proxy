from unittest import TestCase

from hydrogen.signals import default_bus


class TestSignals(TestCase):
    def test_all(self):
        calls = []

        def callback(*args, **kwargs):
            kwargs = list(kwargs.items())
            kwargs.sort(key=lambda t: t[0])
            kwargs = tuple(kwargs)
            calls.append((args, kwargs))

        default_bus.listen("argyle", callback)
        default_bus.listen("argyle*", callback)
        default_bus.listen("argylefish", callback)
        default_bus.listen("argylefish*", callback)
        default_bus.listen("arg", callback)
        default_bus.listen("arg*", callback)
        default_bus.listen("banana", callback)
        default_bus.listen("cantaloupe", callback)

        default_bus.trigger("argyle", 1)
        self.assertEqual(calls, [
            (("argyle", 1), ()),
            (("argyle", 1), ()),
            (("argyle", 1), ()),
        ])

        calls = []
        default_bus.trigger("argylefish", 2)
        self.assertEqual(calls, [
            (("argylefish", 2), ()),
            (("argylefish", 2), ()),
            (("argylefish", 2), ()),
            (("argylefish", 2), ()),
        ])

        calls = []
        default_bus.trigger("argylebird", 3)
        self.assertEqual(calls, [
            (("argylebird", 3), ()),
            (("argylebird", 3), ()),
        ])

        calls = []
        default_bus.trigger("arg", 4)
        self.assertEqual(calls, [
            (("arg", 4), ()),
            (("arg", 4), ()),
        ])

        calls = []
        default_bus.trigger("ar", 5)
        self.assertEqual(calls, [])

        calls = []
        default_bus.trigger("banana", 6)
        self.assertEqual(calls, [
            (("banana", 6), ()),
        ])

        calls = []
        default_bus.trigger("cantaloupe", 7, 8, 9, a=10, b=11, c=12)
        self.assertEqual(calls, [
            (("cantaloupe", 7, 8, 9), (("a", 10), ("b", 11), ("c", 12))),
        ])
