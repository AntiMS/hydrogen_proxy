import io

from .base import BaseTestCase


class TestSimpleBody(BaseTestCase):
    """
    Tests the SimpleBody class.
    """
    def setUp(self):
        """
        Imports the SimpleBody class.
        """
        super().setUp({"CHUNK_SIZE": 16})
        global SimpleBody
        from hydrogen.message.body.simple import SimpleBody

    def test_all(self):
        body = SimpleBody(io.BytesIO(b"a" * 32), 32)
        self.assertEqual(body.length, 32)
        self.assertEqual(body.read(128), b"a" * 32)
        self.assertEqual(body.length, 32)

        body = SimpleBody(io.BytesIO(b"a" * 32))
        self.assertIsNone(body.length)
        self.assertEqual(body.read(128), b"a" * 32)
        self.assertEqual(body.length, 32)

        body = SimpleBody(io.BytesIO(b"a" * 8))
        self.assertEqual(body.length, 8)
        self.assertEqual(body.read(128), b"a" * 8)
        self.assertEqual(body.length, 8)
