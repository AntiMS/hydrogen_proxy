import io

from .base import BaseTestCase


class TestRequest(BaseTestCase):
    """
    Tests the HTTPRequest class.
    """
    def setUp(self):
        """
        Imports the HTTPRequest class.
        """
        super().setUp({"CHUNK_SIZE": 16})
        global HTTPRequest
        from hydrogen.message import HTTPRequest

    def test_properties(self):
        """
        Tests the properties of the HTTPRequest class.
        """
        req = HTTPRequest("127.0.0.1", "GET",
                          "http://www.example.com/path/here.qqq?a=1&b=2&c=3",
                          "HTTP/1.1", {"Argyle-Fishsticks": "Procrastinating"})
        self.assertEqual(req.client_address, "127.0.0.1")
        self.assertEqual(req.method, "GET")
        self.assertEqual(req.url,
                         "http://www.example.com/path/here.qqq?a=1&b=2&c=3")
        self.assertEqual(req.protocol, "http")
        self.assertEqual(req.domain, "www.example.com")
        self.assertEqual(req.port, 80)
        self.assertEqual(req.path, "/path/here.qqq")
        self.assertEqual(req.query, "a=1&b=2&c=3")
        self.assertEqual(req.path_query, "/path/here.qqq?a=1&b=2&c=3")

    def test_get_body(self):
        """
        Tests the get_body() method of the HTTPRequest class.
        """
        req = HTTPRequest("127.0.0.1", "GET",
                          "http://www.example.com/path/here.qqq?a=1&b=2&c=3",
                          "HTTP/1.1", {"Argyle-Fishsticks": "Procrastinating"})
        self.assertEqual(req.body.length, 0)
        self.assertEqual(req.body.read(10), b"")
        self.assertEqual(req.body.length, 0)

        req = HTTPRequest("127.0.0.1", "POST",
                          "http://www.example.com/path/here.qqq?a=1&b=2&c=3",
                          "HTTP/1.1", {"Argyle-Fishsticks": "Procrastinating"},
                          content=io.BytesIO(b"POST body."))
        self.assertEqual(req.body.length, 10)
        self.assertEqual(req.get_input(head=False).read(32), b"POST body.")
        self.assertEqual(req.body.length, 10)

    def test_get_status_line(self):
        """
        Tests the get_status_line() method of the HTTPRequest class.
        """
        req = HTTPRequest("127.0.0.1", "GET",
                          "http://www.example.com/path/here.qqq?a=1&b=2&c=3",
                          "HTTP/1.1", {"Argyle-Fishsticks": "Procrastinating"})
        self.assertEqual(req.get_status_line(),
                         b"GET /path/here.qqq?a=1&b=2&c=3 HTTP/1.1")
        self.assertEqual(req.get_status_line(proxy=True), b"GET "
                         b"http://www.example.com/path/here.qqq?a=1&b=2&c=3 "
                         b"HTTP/1.1")

        req = HTTPRequest("127.0.0.1", "POST",
                          "http://www.example.com/path/here.qqq?a=1&b=2&c=3",
                          "HTTP/1.0", {"Argyle-Fishsticks": "Procrastinating",
                                       "Content-Length": "0"})
        self.assertEqual(req.get_status_line(),
                         b"POST /path/here.qqq?a=1&b=2&c=3 HTTP/1.0")
        self.assertEqual(req.get_status_line(proxy=True), b"POST "
                         b"http://www.example.com/path/here.qqq?a=1&b=2&c=3 "
                         b"HTTP/1.0")
