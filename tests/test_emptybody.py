from unittest import TestCase

from hydrogen.message.body.empty import EmptyBody


class TestEmptyBody(TestCase):
    """
    Tests the EmptyBody class.
    """
    def test_all(self):
        class FakeContent:
            def __init__(self):
                self.closed = False

            def close(self):
                self.closed = True
        content = FakeContent()
        body = EmptyBody(content, 100)

        self.assertEqual(body.length, 0)
        self.assertEqual(body.read(1024), b"")
        self.assertEqual(body.read(1024), b"")
        self.assertFalse(content.closed)
        body.close()
        self.assertTrue(content.closed)
