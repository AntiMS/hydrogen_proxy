from unittest import TestCase

from hydrogen.helpers import module
from hydrogen.init import init_chains
from hydrogen.signals import default_bus
from hydrogen.contrib.admin.registry import default_admin


class TestModule:
    def __init__(self, val, args=None, kwargs=None, hasinit=None,
                 initcalled=False):
        assert not (hasinit is True and initcalled)
        if initcalled:
            hasinit = False
        if hasinit is None:
            hasinit = True
        self.val = val
        self.args = args
        self.kwargs = kwargs
        self.initcalled = initcalled
        if hasinit:
            def init():
                self.initcalled = True
            self.init = init

    def __eq__(self, s2):
        if not isinstance(s2, TestModule):
            return False
        out = self.val == s2.val
        out = out and self.args == s2.args
        out = out and self.kwargs == s2.kwargs
        out = out and self.initcalled == s2.initcalled
        return out

    def __call__(self, *args, **kwargs):
        return TestModule(self.val, args, kwargs)

    def __repr__(self):
        return "S{}({}, {}, {})".format("c" if self.initcalled else "",
                                        repr(self.val), repr(self.args),
                                        repr(self.kwargs))

    def __hash__(self):
        out = 0
        for t in (self.val, self.args, self.kwargs, self.initcalled):
            if isinstance(t, dict):
                t = tuple(t.items())
            out ^= hash(t)
        return out


M = TestModule


def Mc(*args, **kwargs):
    return TestModule(*args, initcalled=True, **kwargs)


_CHAINS = {
    "angle": M("angle"),
    "bacon": (M("bacon"), (1,), {"a": 2}),
    "canon": (M("canon"), (3,)),
    "dream": (M("dream"),),
    "eagle": {"class": M("eagle"), "args": (4,), "kwargs": {"b": 5}},
    "fight": {"class": M("fight"), "args": (6,)},
    "grunt": {"class": M("grunt"), "kwargs": {"c": 7}},
    "hinge": {"class": M("hinge")},
    "igloo": module(class_=M("igloo"), args=(8,), kwargs={"d": 9}),
    "jacks": module(class_=M("jacks"), args=(10,)),
    "koala": module(class_=M("koala"), kwargs={"e": 11}),
    "lemur": module(class_=M("lemur")),
    "magic": module(M("magic"), (12,), {"f": 13}),
    "norse": module("angle"),
    "oscar": (M("oscar"), (module("angle"),), {"g": module("angle")}),
    "purse": {
        "class": M("purse1"),
        "args": (module(
            M("purse2"),
            (14,),
            {"h": 15}
        ),),
        "kwargs": {"i": module(
            M("purse3"),
            (16,),
            {"j": 17}
        )}
    },
    "quest": M("quest", hasinit=False),
}
_RESULT = {
    "angle": Mc("angle", (), {}),
    "bacon": Mc("bacon", (1,), {"a": 2}),
    "canon": Mc("canon", (3,), {}),
    "dream": Mc("dream", (), {}),
    "eagle": Mc("eagle", (4,), {"b": 5}),
    "fight": Mc("fight", (6,), {}),
    "grunt": Mc("grunt", (), {"c": 7}),
    "hinge": Mc("hinge", (), {}),
    "igloo": Mc("igloo", (8,), {"d": 9}),
    "jacks": Mc("jacks", (10,), {}),
    "koala": Mc("koala", (), {"e": 11}),
    "lemur": Mc("lemur", (), {}),
    "magic": Mc("magic", (12,), {"f": 13}),
    "norse": Mc("angle", (), {}),
    "oscar": Mc("oscar", (Mc("angle", (), {}),), {"g": Mc("angle", (), {})}),
    "purse": Mc("purse1", (Mc("purse2", (14,), {"h": 15}),),
                {"i": Mc("purse3", (16,), {"j": 17})}),
    "quest": Mc("quest", (), {}, hasinit=False),
}
_SIGNALS = [
    (("module_created.angle", "angle", Mc("angle", (), {}), (), {}, ()), ()),
    (("module_created.bacon", "bacon", Mc("bacon", (1,), {"a": 2}), (1,),
      {"a": 2}, ()), ()),
    (("module_created.canon", "canon", Mc("canon", (3,), {}), (3,), {}, ()),
     ()),
    (("module_created.dream", "dream", Mc("dream", (), {}), (), {}, ()), ()),
    (("module_created.eagle", "eagle", Mc("eagle", (4,), {"b": 5}), (4,),
      {"b": 5}, ()), ()),
    (("module_created.fight", "fight", Mc("fight", (6,), {}), (6,), {}, ()),
     ()),
    (("module_created.grunt", "grunt", Mc("grunt", (), {"c": 7}), (), {"c": 7},
      ()), ()),
    (("module_created.hinge", "hinge", Mc("hinge", (), {}), (), {}, ()), ()),
    (("module_created.igloo", "igloo", Mc("igloo", (8,), {"d": 9}), (8,),
      {"d": 9}, ()), ()),
    (("module_created.jacks", "jacks", Mc("jacks", (10,), {}), (10,), {}, ()),
     ()),
    (("module_created.koala", "koala", Mc("koala", (), {"e": 11}), (),
      {"e": 11}, ()), ()),
    (("module_created.lemur", "lemur", Mc("lemur", (), {}), (), {}, ()), ()),
    (("module_created.magic", "magic", Mc("magic", (12,), {"f": 13}), (12,),
      {"f": 13}, ()), ()),
    (("module_created.oscar", "oscar", Mc("oscar",
                                          (Mc("angle", (), {}),),
                                          {"g": Mc("angle", (), {})}),
      (Mc("angle", (), {}),), {"g": Mc("angle", (), {})},
      (Mc("angle", (), {}), Mc("angle", (), {}))), ()),
    (("module_created.purse", "purse",
      Mc("purse1", (Mc("purse2", (14,), {"h": 15}),),
         {"i": Mc("purse3", (16,), {"j": 17})}),
      (Mc("purse2", (14,), {"h": 15}),), {"i": Mc("purse3", (16,), {"j": 17})},
      (Mc("purse2", (14,), {"h": 15}), Mc("purse3", (16,), {"j": 17}))), ()),
    (("module_created.quest", "quest", Mc("quest", (), {}), (), {}, ()), ()),
    (("module_created.<anon>", None, Mc("purse2", (14,), {"h": 15}), (14,),
      {"h": 15}, ()), ()),
    (("module_created.<anon>", None, Mc("purse3", (16,), {"j": 17}), (16,),
      {"j": 17}, ()), ()),
]


class TestInit(TestCase):
    """
    Tests the "init_chains" method in hydrogen.init .
    """
    def test_init(self):
        signals = []

        def signal_handler(*args, **kwargs):
            kwargs = list(kwargs.items())
            kwargs.sort(key=lambda t: t[0])
            kwargs = tuple(kwargs)
            signals.append((args, kwargs))

        default_bus.listen("module_created.*", signal_handler)

        self.maxDiff = 3000

        # Avoids an assertion error related to the default admin registerer.
        default_admin.reset()

        self.assertEqual(init_chains(_CHAINS), _RESULT)

        self.assertEqual(len(signals), len(_SIGNALS))

        for s in signals:
            self.assertIn(s, _SIGNALS)
