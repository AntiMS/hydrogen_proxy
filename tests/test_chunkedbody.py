import io

from .base import BaseTestCase


class TestChunkedBody(BaseTestCase):
    """
    Tests the ChunkedBody class.
    """
    def setUp(self):
        """
        Imports the ChunkedBody class.
        """
        super().setUp({"CHUNK_SIZE": 16})
        global ChunkedBody
        from hydrogen.message.body.chunked import ChunkedBody

    def test_all(self):
        content = b"1\r\na\r\nf\r\nbcdefghijklmnop\r\n24\r\nqrstuvwxyz" \
                  b"ABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n0\r\n\r\n"
        body = ChunkedBody(io.BytesIO(content))
        self.assertIsNone(body.length)
        self.assertEqual(body.read(10), b"abcdefghij")
        self.assertEqual(body.read(128), b"klmnopqrstuvwxyzABCDEFGHIJKLMNOPQRS"
                                         b"TUVWXYZ")
        self.assertEqual(body.length, 52)
