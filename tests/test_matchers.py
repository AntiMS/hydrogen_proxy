from unittest import TestCase

from hydrogen.matchers.base import BaseMatcher
from hydrogen.matchers.regex import RegexMatcher
from hydrogen.matchers import url, path, domain, protocol, tag


class TestMatchers(TestCase):
    """
    Tests the matcher classes.
    """
    def test_regex(self):
        """
        Tests the RegexMatcher class.
        """
        class TestMatcher(RegexMatcher):
            def get_matchable_string(self, request):
                """
                Takes the string to be matched instead of a request.
                """
                return request

        self.assertTrue(TestMatcher(r"^$").matches(""))
        self.assertTrue(TestMatcher(r"argyle").matches(
            "onetwothreeargylethreetwoone"))
        self.assertTrue(TestMatcher(r"^.*$").matches("!@#$%^&*()"))

        self.assertFalse(TestMatcher(r"^$").matches("one"))
        self.assertFalse(TestMatcher(r"argyle").matches(
            "onetwothreeelygrathreetwoone"))
        self.assertFalse(TestMatcher(r"^[^q]+$").matches("aqueus"))

    def test_regex_subclasses(self):
        """
        Tests the classes URLMatcher, PathMatcher, DomainMatcher, and
        ProtocolMatcher.
        """
        class FakeRequest:
            def __init__(self, url, path, domain, protocol):
                self.url = url
                self.path = path
                self.domain = domain
                self.protocol = protocol

        r = FakeRequest("http://www.example.com/path/here.txt",
                        "/path/here.txt", "www.example.com", "http")

        # URLMatcher
        self.assertTrue(url(r"^http://www\.example\.com/path/here\.txt$"
                            ).matches(r))
        self.assertTrue(url(r"www\.example\.com").matches(r))
        self.assertTrue(url(r"^http://").matches(r))
        self.assertTrue(url(r"com/").matches(r))
        self.assertTrue(url(r"here\.txt$").matches(r))

        self.assertFalse(url(r"^$").matches(r))
        self.assertFalse(url(r"argyle\.example\.com").matches(r))
        self.assertFalse(url(r"^https://").matches(r))
        self.assertFalse(url(r"net/").matches(r))
        self.assertFalse(url(r"there\.txt$").matches(r))

        # PathMatcher
        self.assertTrue(path(r"^/path/here\.txt$").matches(r))
        self.assertTrue(path(r"here\.txt$").matches(r))
        self.assertTrue(path(r"^/path").matches(r))
        self.assertTrue(path(r"th/he").matches(r))
        self.assertTrue(path(r"txt$").matches(r))

        self.assertFalse(path(r"^$").matches(r))
        self.assertFalse(path(r"there\.txt$").matches(r))
        self.assertFalse(path(r"^/parse").matches(r))
        self.assertFalse(path(r"th/the").matches(r))
        self.assertFalse(path(r"tqt$").matches(r))

        # DomainMatcher
        self.assertTrue(domain(r"^www\.example\.com$").matches(r))
        self.assertTrue(domain(r"\.com$").matches(r))
        self.assertTrue(domain(r"^www\.").matches(r))
        self.assertTrue(domain(r"\.example\.").matches(r))

        self.assertFalse(domain(r"^$").matches(r))
        self.assertFalse(domain(r"\.co$").matches(r))
        self.assertFalse(domain(r"^mail\.").matches(r))
        self.assertFalse(domain(r"\.exxample\.").matches(r))

        # ProtocolMatcher
        self.assertTrue(protocol(r"^http$").matches(r))
        self.assertTrue(protocol(r"^ht").matches(r))
        self.assertTrue(protocol(r"tp$").matches(r))
        self.assertTrue(protocol(r"tt").matches(r))

        self.assertFalse(protocol(r"^$").matches(r))
        self.assertFalse(protocol(r"^hq").matches(r))
        self.assertFalse(protocol(r"qp$").matches(r))
        self.assertFalse(protocol(r"tq").matches(r))

    def test_tag(self):
        """
        Tests the tag matcher.
        """
        class FakeRequest:
            def __init__(self, tags):
                self.tags = tags

        r = FakeRequest({"a", "b", "c", "d", "e"})

        self.assertTrue(tag("a", "b", "c", "d", "e").matches(r))
        self.assertTrue(tag("a").matches(r))
        self.assertTrue(tag("b", "e").matches(r))
        self.assertTrue(tag("e", "d", "c", "b").matches(r))

        self.assertFalse(tag("q").matches(r))
        self.assertFalse(tag("a", "b", "c", "d", "e", "f").matches(r))
        self.assertFalse(tag("e", "f").matches(r))
        self.assertFalse(tag("z", "y", "x").matches(r))

    def test_combine(self):
        """
        Tests the overridden | and & operators of matchers.
        """
        class TrueMatcher(BaseMatcher):
            def matches(self, *args, **kwargs):
                return True

        class FalseMatcher(BaseMatcher):
            def matches(self, *args, **kwargs):
                return False

        true = TrueMatcher()
        false = FalseMatcher()

        # Or
        self.assertTrue((true | true).matches(None))
        self.assertTrue((true | false).matches(None))
        self.assertTrue((false | true).matches(None))
        self.assertFalse((false | false).matches(None))

        # And
        self.assertTrue((true & true).matches(None))
        self.assertFalse((true & false).matches(None))
        self.assertFalse((false & true).matches(None))
        self.assertFalse((false & false).matches(None))
