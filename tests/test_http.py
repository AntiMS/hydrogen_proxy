from http.client import parse_headers
from io import BytesIO
import socket
import ssl

from .base import BaseTestCase


class TestHTTPSender(BaseTestCase):
    """
    Tests the HTTPSender.
    """
    def setUp(self):
        super().setUp({"VERBOSE": False, "CHUNK_SIZE": 100})

        global http, HTTPRequest, set_socket

        from hydrogen.senders import http
        from hydrogen.message import HTTPRequest

        def set_socket(ret):
            if isinstance(ret, bytes):
                ret = BytesIO(ret)

            class Socket:
                def __init__(self):
                    self.wrapped = False
                    self.sent = b""
                    self.addr = None
                    self.mode = None

                def connect(self, addr):
                    self.addr = addr

                def sendall(self, chunk):
                    self.sent += chunk

                def makefile(self, mode):
                    self.filemode = mode
                    return ret

            sock = Socket()

            def get_socket():
                return sock

            socket.socket = get_socket

            return sock

        class Context:
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

            def wrap_socket(self, sock, server_hostname):
                sock.wrapped = True
                return sock

        ssl.create_default_context = Context

    def test_http_get(self):
        """
        Tests HTTP GET requests.
        """
        hsend = http()

        request = HTTPRequest(("127.0.0.1", 1234), "GET",
                              "http://www.example.com/testing",
                              "HTTP/1.1",
                              parse_headers(BytesIO(b"A: B\r\nC: D\r\n\r\n")),
                              BytesIO(b""))
        sock = set_socket(b"HTTP/1.1 200 Ok\r\nContent-Length: 15\r\n\r\n"
                          b"This is a test.")
        hsend(request)

        self.assertFalse(sock.wrapped)
        self.assertEqual(len(sock.addr), 2)
        self.assertEqual(sock.addr[0], "www.example.com")
        self.assertEqual(sock.addr[1], 80)
        self.assertEqual(sock.filemode, "rb")
        self.assertEqual(sock.sent, b"GET /testing HTTP/1.1\r\nA: B\r\nC: D"
                         b"\r\n\r\n")

    def test_htts_get(self):
        """
        Tests HTTPS GET requests.
        """
        hsend = http()

        request = HTTPRequest(("127.0.0.1", 1234), "GET",
                              "https://www.example.com/testing",
                              "HTTP/1.1",
                              parse_headers(BytesIO(b"A: B\r\nC: D\r\n\r\n")),
                              BytesIO(b""))
        sock = set_socket(b"HTTP/1.1 200 Ok\r\nContent-Length: 15\r\n\r\n"
                          b"This is a test.")
        hsend(request)

        self.assertTrue(sock.wrapped)
        self.assertEqual(len(sock.addr), 2)
        self.assertEqual(sock.addr[0], "www.example.com")
        self.assertEqual(sock.addr[1], 443)
        self.assertEqual(sock.filemode, "rb")
        self.assertEqual(sock.sent, b"GET /testing HTTP/1.1\r\nA: B\r\nC: D"
                         b"\r\n\r\n")

    def test_http_post(self):
        """
        Tests HTTP POST requests.
        """
        hsend = http()

        request = HTTPRequest(("127.0.0.1", 1234), "POST",
                              "http://www.example.com/testing",
                              "HTTP/1.1",
                              parse_headers(BytesIO(b"A: B\r\nContent-Length: "
                                                    b"15\r\n\r\n")),
                              BytesIO(b"This is a test."))
        sock = set_socket(b"HTTP/1.1 200 Ok\r\nContent-Length: 15\r\n\r\n"
                          b"This is a test.")
        hsend(request)

        self.assertFalse(sock.wrapped)
        self.assertEqual(len(sock.addr), 2)
        self.assertEqual(sock.addr[0], "www.example.com")
        self.assertEqual(sock.addr[1], 80)
        self.assertEqual(sock.filemode, "rb")
        self.assertEqual(sock.sent, b"POST /testing HTTP/1.1\r\nA: B\r\n"
                         b"Content-Length: 15\r\n\r\nThis is a test.")

    def test_https_post(self):
        """
        Tests HTTPS POST requests.
        """
        hsend = http()

        request = HTTPRequest(("127.0.0.1", 1234), "POST",
                              "https://www.example.com/testing",
                              "HTTP/1.1",
                              parse_headers(BytesIO(b"A: B\r\nContent-Length: "
                                                    b"15\r\n\r\n")),
                              BytesIO(b"This is a test."))
        sock = set_socket(b"HTTP/1.1 200 Ok\r\nContent-Length: 15\r\n\r\n"
                          b"This is a test.")
        hsend(request)

        self.assertTrue(sock.wrapped)
        self.assertEqual(len(sock.addr), 2)
        self.assertEqual(sock.addr[0], "www.example.com")
        self.assertEqual(sock.addr[1], 443)
        self.assertEqual(sock.filemode, "rb")
        self.assertEqual(sock.sent, b"POST /testing HTTP/1.1\r\nA: B\r\n"
                         b"Content-Length: 15\r\n\r\nThis is a test.")
