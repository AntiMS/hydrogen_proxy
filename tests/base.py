import unittest

from hydrogen.conf import add_layers


class BaseTestCase(unittest.TestCase):
    """
    Sets up the settings and options modules. (Sets them empty by default,
    object by defualt, but this behavior can be overriden by subclasses.)
    """
    def setUp(self, settings={}):
        add_layers(settings)
