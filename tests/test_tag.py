from unittest import TestCase

from hydrogen.handlers import tagreq, tagres


class FakeTransaction:
    def __init__(self, tags):
        self.tags = tags


class FakeHandler:
    def _normalize_transaction(self, transaction):
        if transaction is not None and not isinstance(transaction,
                                                      FakeTransaction):
            return FakeTransaction(transaction)
        return transaction

    def __init__(self, response=None):
        self.response = self._normalize_transaction(response)

    def __call__(self, request):
        self.request = self._normalize_transaction(request)
        return self.response


class TestTaggingHandlers(TestCase):
    """
    Tests the request and response tagging handlers.
    """
    def test_request_tagging_handlers(self):
        """
        Tests the RequestTaggingHandler.
        """
        child = FakeHandler()
        handler = tagreq(child, "a", "b", "c")
        handler(FakeTransaction({"c", "d", "e"}))
        tags = child.request.tags

        self.assertEqual(tags, {"a", "b", "c", "d", "e"})

        handler = tagreq(child, "noproxy", "socks4a", "argyle")
        handler(FakeTransaction({"entry_one", "api", "argyle"}))
        tags = child.request.tags

        self.assertEqual(tags, {"noproxy", "socks4a", "argyle", "entry_one",
                                "api"})

    def test_response_tagging_handler(self):
        """
        Tests the ResponseTaggingHandler.
        """
        child = FakeHandler(FakeTransaction({"a", "b", "c"}))
        handler = tagres(child, "c", "d", "e")
        tags = handler(None).tags

        self.assertEqual(tags, {"a", "b", "c", "d", "e"})

        child = FakeHandler(FakeTransaction({"noproxy", "socks4a", "argyle"}))
        handler = tagres(child, "entry_one", "api", "argyle")
        tags = handler(None).tags

        self.assertEqual(tags, {"noproxy", "socks4a", "argyle", "entry_one",
                                "api"})
