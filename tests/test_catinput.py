from io import BytesIO
from unittest import TestCase

from hydrogen.message.catinput import CatInput


class TestCatInput(TestCase):
    """
    Tests the CatInput class.
    """
    def test_all(self):
        b1 = b"This "
        b2 = b"is "
        i1 = BytesIO(b"a ")
        i2 = BytesIO(b"test!")
        self.assertEqual(CatInput(b1, b2, i1, i2).read(15), b"This is a test!")
